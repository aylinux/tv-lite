
#include <wx/filedlg.h>

#include "subscription.h"
#include "subscriptiondialog.h"
#include "subscriptionlistdialog.h"
#include "database.h"
#include "debug.h"

using namespace tvlite;

CSubscriptionDialog::CSubscriptionDialog(wxWindow* parent):CSubscriptionDialogBase(parent)
{
}

CSubscriptionDialog::~CSubscriptionDialog()
{
}

void CSubscriptionDialog::SetNameField(wxString param)
{
   m_Name->SetValue(param);
}

const wxString CSubscriptionDialog::GetNameField() const
{
   return m_Name->GetValue();
}

void CSubscriptionDialog::SetURLField(wxString param)
{
   m_URLText->SetValue(param);
}

const wxString CSubscriptionDialog::GetURLField() const
{
   return m_URLText->GetValue();
}


void CSubscriptionDialog::OnCancelButtonClicked( wxCommandEvent& event )
{
   event.Skip();
   EndModal(wxID_CANCEL);
}

void CSubscriptionDialog::OnOkButtonClicked( wxCommandEvent& event )
{
      wxString nameText = m_Name->GetValue();
      wxString urlText = m_URLText->GetValue().Trim().Trim(false);
      DBG_INFO("URL is %s", (const char*)urlText.utf8_str());

      if (nameText == m_subscriptionName && urlText == m_subscriptionURL)
      {
         return;
      }
      if (!m_Name->IsShown()  &&  CSubscription::FindCachedData(urlText) != "")
      {
         wxMessageBox(_("Found another subscription with the same URL!"), _("Error"), wxOK | wxICON_ERROR, NULL);
         return;
      }
      wxString deleteFileName = CSubscription::FindCachedData(m_subscriptionURL);
      wxString cachedFileName = CSubscription::FindCachedData(urlText);
      CSubscription sub(urlText);
      CSubscriptionInfo &subInfo = sub.GetSubscriptionInfo();
      subInfo.name = nameText;
      if (sub.ReadData(cachedFileName != ""))
      {
         return;
      }
      wxString path;
      sub.SaveDataToCache(path);
      
      if (deleteFileName != "")
      {
         wxRemoveFile(deleteFileName);
      }
      ((MainFrame*)wxGetApp().GetTopWindow())->RefreshInterface(path);
      ((CSubscriptionListDialog*)GetParent())->Repopulate(path);   
      EndModal(wxID_OK);
   
} 


void CSubscriptionDialog::OnBrowseButtonClick(wxCommandEvent& event)
{
    wxFileDialog openFileDialog(this, _("Open XYZ file"), "", "",
                       wxFileSelectorDefaultWildcardStr, wxFD_OPEN|wxFD_FILE_MUST_EXIST);
    if (openFileDialog.ShowModal() == wxID_CANCEL)
    {
       return;
    }
    m_URLText->SetValue(openFileDialog.GetPath());                      
}


int CSubscriptionDialog::ShowModal()
{
   int parentWidth, parentHeight, width, height;
   GetParent()->GetSize(&parentWidth, &parentHeight);
   Fit();
   GetSize(&width,&height);
   SetSize(parentWidth, height);
   m_subscriptionName = m_Name->GetValue();
   m_subscriptionURL  = m_URLText->GetValue();
   return CSubscriptionDialogBase::ShowModal();
}

int CSubscriptionDialog::ShowURLOnly()
{
   
   m_NameLabel->Hide();
   m_Name->Hide();
   return ShowModal();
}

