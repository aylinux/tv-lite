#ifndef CSUBSCRIPTION_H
#define CSUBSCRIPTION_H
#ifdef __WXMSW__
    #include <windows.h>
    #include <rpc.h>
    #include "wx/msw/ole/uuid.h" 
#else
    #include <uuid/uuid.h>
    typedef unsigned char uuid_string_t[UUID_STR_LEN]; 
#endif 
#include <wx/uri.h>
#include <wx/event.h>
#include "subscriptioninfo.h"
#include "channel.h"
#include "database.h"

namespace tvlite
{
class CSubscription;

WX_DECLARE_OBJARRAY(CSubscription, TSubscriptionList);  
   
class CSubscription
{
public:
   CSubscription();
   CSubscription(wxString uri);
   virtual ~CSubscription();
   int  ReadData(bool fromCache = false);
   TChannelList GetChannelList(wxString searchString = "", bool sorted = false, bool asc = true);
   void SetChannelList(TChannelList chanList); 
   CSubscriptionInfo& GetSubscriptionInfo(); 
   void SetSubscriptionInfo(CSubscriptionInfo subscriptionInfo);
   static void GetCachedData(TSubscriptionList &subList);
   static wxString FindCachedData(wxString url);
   int SaveDataToCache();
   int SaveDataToCache(wxString &idStr);
   int SaveDataToListDir();
   int SaveDataToListDir(wxString &idStr);
   int SaveData(wxString path, bool keepuri = false);
   wxString GetURI();
   unsigned int GetNumberOfChannels();
   unsigned int GetNumberOfSources();
   static int Compare( CSubscription **item1, CSubscription **item2);
   
protected:
   int  ParseData(wxString dbFile);
   int  ParseDbData(wxString dbFile);
   int  ParseM3UData(wxString dbFile);
   wxString GenUUID();
   
   CSubscriptionInfo m_subscriptionInfo;
   TChannelList m_chanlist;
   wxString m_uri;
   wxString GenerateTempFileName();
   
};


 
} //namespace

#endif // CSUBSCRIPTION_H

