#ifndef CDATABASE_H
#define CDATABASE_H

#include <wx/string.h>
#include "subscriptioninfo.h"
#include <sqlite3.h>
//local
#include "channel.h"


#define E_DB_OK 0
#define E_DB_CREATE -1
#define E_DB_QUERY -2
 

namespace tvlite
{

class CDataBase
{
public:
   CDataBase(wxString name) : m_name(name), m_db(NULL) {} ;
   //no implicit constructor
   CDataBase() = delete;
   //no copy possible
   CDataBase(const CDataBase &) = delete;
   //no move possible
   CDataBase(const CDataBase &&) = delete;
   int Init();
   int GetInfoTableData(CSubscriptionInfo &dataBaseInfo);
   int SetInfoTableData(CSubscriptionInfo &dataBaseInfo);
   int UpdateInfoTableData(CSubscriptionInfo &dataBaseInfo);
   int GetTVTableData  (TChannelList &channelList);
   int SetTVTableData  (TChannelList &channelList);
   int AddChannel(CChannel &ch);
   int UpdateChannel(CChannel &ch);
   int DeleteChannel(CChannel &ch);
   ~CDataBase() {sqlite3_close(m_db);};
private:
   wxString m_name;
   sqlite3 *m_db;
   int InitInfoTable();
   int InitTVTable();
   int InitRadioTable();
   int InitTable(wxString name, wxString stmt = "", bool create = false);
   int DeleteTable(wxString name);
   int ResetTVTableData();
   int ResetInfoTableData();
   static int tab_callback (void *data, int nr_cols, char **arg, char **colname);
   static int info_callback(void *data, int nr_cols, char **arg, char **colname);
   static int channel_callback(void *data, int nr_cols, char **arg, char **colname);
};

struct TCallbackData
{
   TCallbackData(CDataBase *_instance, wxString &_tablename, bool _tablefound = NULL): 
      instance(_instance), tablename(_tablename), tablefound(_tablefound) 
      {} ;   
   TCallbackData() = delete;
   
   CDataBase *instance;
   wxString  tablename;
   bool tablefound;
};

}

#endif // CDATABASE_H
