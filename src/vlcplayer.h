#ifndef CVLCPLAYER_H
#define CVLCPLAYER_H

#ifdef __WXGTK__
    #include <gdk/gdkx.h>
    #include <gtk/gtk.h>
    //#include <wx/gtk/win_gtk.h>
    #define GET_XID(window) GDK_WINDOW_XWINDOW(static_cast<GtkWidget *>(window->GetHandle()))
#endif

#include <wx/window.h>
#include <wx/string.h>
#include <wx/timer.h>
#include <vlc/vlc.h>

DECLARE_EVENT_TYPE(playerEVT, -1)
DECLARE_EVENT_TYPE(seekEVT, -1)

namespace tvlite
{

enum EVLCRecordState
{
   rsNone = 0,
   rsFile,
   rsStream  
};   

enum ETrackType
{
   etNone = -1,
   etVideo = 0,
   etAudio,
   etSubtitle,
   etAll
};

class CTrackInfo
{
   private:
      int m_index;
      wxString m_info;
      ETrackType m_type;
   public:   
      CTrackInfo():m_index(-1), m_type(etNone) {} ;
      CTrackInfo(int index, ETrackType type, wxString info):m_index(index), m_info(info), m_type(type) {} ;
      void SetIndex(int index) {m_index = index;};
      int GetIndex() { return m_index;};
      void SetInfo(wxString info) {m_info = info;};
      wxString GetInfo() { return m_info;};
      void SetType(ETrackType type) {m_type = type;};
      ETrackType GetType() { return m_type; } ;
      
} ;

WX_DECLARE_HASH_MAP(ETrackType, int , wxIntegerHash, wxIntegerEqual, TTrackMap );

WX_DECLARE_OBJARRAY(CTrackInfo, TTrackInfoList);
     
class CVlcPlayer:public wxEvtHandler
{
private:
   CVlcPlayer(const CVlcPlayer& rhs) = delete;
   CVlcPlayer& operator=(const CVlcPlayer& rhs) = delete;
   
   libvlc_instance_t *m_vlc_inst;
   libvlc_media_player_t *m_media_player;
   libvlc_event_manager_t *m_vlc_evt_man;
#ifdef __WXGTK__
   GdkWindow * m_embedded;
#endif   
#ifdef __WXMSW__
   wxWindow *m_wnd;
#endif   
   wxString m_location;
   bool m_wasStopped;
   EVLCRecordState m_rState;
   wxString m_recordFileName;
   TTrackMap m_trackMap;
protected:  
   wxEvtHandler *m_pparent;
   static void VlcEvtCallback(const struct libvlc_event_t *p_event, void *p_data);

public:
   CVlcPlayer() = delete;
   CVlcPlayer(wxEvtHandler *parent);
   ~CVlcPlayer();
   void Init(wxWindow* wnd);
   void SetEmbedded();
   void SetFull();
   void Play(wxString location);
   void Play();
   void Stop();
   void Pause();
   void Seek(float position);
   float GetPosition();
   void SetVolume(int value);
   void SetAspectRatio(wxString aspectRatio);
   bool WasStopped();
   void SetRecordState(EVLCRecordState state);
   void SetRecordFileName(wxString name);
   wxString GetRecordFileName();
   EVLCRecordState GetRecordState();
   int GetTracks(TTrackInfoList &tracklist, ETrackType trackType);
   void SetTrack(int trackid, ETrackType trackType);
   void RestoreTracks();
   void ResetTracks();
};

}

#endif // CVLCPLAYER_H
