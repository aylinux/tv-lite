
#include <rapidjson/document.h>     // rapidjson's DOM-style API
#include <rapidjson/prettywriter.h> // for stringify JSON
#include <cstdio>
#include "channel.h"
#include "debug.h"

using namespace rapidjson;
using namespace std;
using namespace tvlite;

#define E_OK 0
#define E_PARSE -1

#include <wx/arrimpl.cpp>
WX_DEFINE_OBJARRAY(streamurltype);

CChannel::CChannel(wxString id, wxString name, streamurltype streamurls, wxString iconfile): 
      m_id(id), m_name(name), m_streamurls(streamurls)
{
}

CChannel::CChannel()
{
};

CChannel::~CChannel()
{

}

wxString CChannel::GetName()
{
   return m_name;
}

void CChannel::SetName(wxString name)
{
   m_name = name;
}

wxString CChannel::GetId()
{
   return m_id;
}

void CChannel::SetId(wxString id)
{
   m_id = id;
}

streamurltype CChannel::GetStreamURLs()
{
   return m_streamurls;
}

streamurltype CChannel::GetAudioChannels()
{
   return m_audiochannels;
}

void CChannel::SetStreamURLs(streamurltype s)
{
   m_streamurls = s; 
}

void CChannel::SetAudioChannels(streamurltype s)
{
   m_audiochannels = s; 
}


int CChannel::ConvertFromJSONArray(wxString s, streamurltype& sURLs)
{  
   Document d;
   int result = E_OK;
   const char* js = s.utf8_str();
   if (d.Parse(js).HasParseError())
   {
     result = E_PARSE;
   }
   if (result == E_OK)
   {
      if (!d.IsArray())
      {
        result = E_PARSE;
      }   
   }
   if (result == E_OK)
   {
      const Value& refURLs = d;
      for (Value::ConstValueIterator itr = refURLs.Begin(); itr != refURLs.End(); ++itr)
      {
         sURLs.Add(itr->GetString());
         
      }
      for (size_t i = 0; i < sURLs.GetCount(); i++)
      {
         //DBG_INFO("Found url: %s", (const char*)sURLs[i].utf8_str());
      }
   }
   return result;
}


wxString CChannel::ConvertToJSONArray(streamurltype& sURLs)
{  
   wxString s = "[";
   for (size_t i = 0; i < sURLs.GetCount(); i++)
   {
      s = s + "\"" + sURLs[i] + "\""; 
      if (i != sURLs.GetCount() - 1)
      {
         s = s + ", ";
      }
   }
   s = s + "]";
   return s;
}   

wxString CChannel::ConvertToJSONParam(streamurltype& sURLs)
{
   wxString s = "{";
   for (size_t i = 0; i < sURLs.GetCount(); i++)
   {
      s = s + "\"" + sURLs[i] + "\":\"\""; 
      if (i != sURLs.GetCount() - 1)
      {
         s = s + ", ";
      }
   }
   s = s + "}";
   return s;
   
}

int CChannel::CompareAsc( CChannel **item1, CChannel **item2)
{
  wxString s1 = (*item1)->GetName();
  wxString s2 = (*item2)->GetName();

  return strcmp((const char*)s1.utf8_str(), (const char*)s2.utf8_str());
}

int CChannel::CompareDesc( CChannel **item1, CChannel **item2)
{
  wxString s1 = (*item1)->GetName();
  wxString s2 = (*item2)->GetName();

  return (-1)*strcmp((const char*)s1.utf8_str(), (const char*)s2.utf8_str());
}