#include "localdialog.h"
#include <wx/msgdlg.h>

using namespace tvlite;

CLocalDialog::CLocalDialog(wxWindow* parent):CLocalDialogBase(parent)
{
}

CLocalDialog::~CLocalDialog()
{
}

void CLocalDialog::SetData(CSubscriptionInfo& info)
{
  m_Name->SetValue(info.name);
  m_Author->SetValue(info.author);
  m_urlText->SetValue(info.url);
  m_versionText->SetValue(info.version);  
}

void CLocalDialog::GetData(CSubscriptionInfo& info)
{
   info.name = m_Name->GetValue().Trim().Trim(false);
   info.author = m_Author->GetValue().Trim().Trim(false);
   info.url = m_urlText->GetValue().Trim().Trim(false);
   info.version = m_versionText->GetValue().Trim().Trim(false);
}

bool CLocalDialog::ValidateData()
{
   if (m_Name->GetValue().Trim().Trim(false) =="")
   {
      wxMessageBox(_("Please input a valid name"), _("Validation error"), wxOK|wxICON_EXCLAMATION, this);
      return false;
   }
   
   return true;
}

void CLocalDialog::OnOkClicked( wxCommandEvent& event )
{
   if (ValidateData())
   {
      EndModal(wxID_OK);
   }
}