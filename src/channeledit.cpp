#include "channeledit.h"
#include "channel.h"
#include <wx/msgdlg.h>

using namespace tvlite;

CChannelEdit::CChannelEdit(wxWindow* parent):CChannelEditBase(parent)
{
    int width, height;
    m_addressList->GetSize(&width, &height);
    m_addressList->AppendTextColumn(_("Addresses"), wxDATAVIEW_CELL_EDITABLE, width);
}

CChannelEdit::~CChannelEdit()
{
}

void CChannelEdit::SetData(CChannel *channel)
{
   *m_channelName << channel->GetName();
   for (size_t i = 0; i < channel->GetStreamURLs().GetCount(); i++)
   {
      wxVector<wxVariant> data;
      data.push_back(channel->GetStreamURLs()[i]);
      m_addressList->AppendItem(data);
   }
}

void CChannelEdit::GetData(CChannel *channel)
{
   streamurltype s;
   for (int i = 0; i < m_addressList->GetItemCount(); i++)
   {
      wxString addressString = m_addressList->GetTextValue(i,0).Trim().Trim(false);
      if (addressString != "" && addressString != "Click here to input the address...")
      {
         s.Add(addressString);
      }
      
   }
   channel->SetStreamURLs(s);
   channel->SetName(m_channelName->GetValue());
   
}

bool CChannelEdit::ValidateData()
{
   if (m_channelName->GetValue() == "")
   {
      wxMessageBox(_("Please input a valid name"), _("Validation error"), wxOK|wxICON_EXCLAMATION, this);
      return false;
   }
   int count = 0;
   for (int i = 0; i < m_addressList->GetItemCount(); i++)
   {
      wxString addressString = m_addressList->GetTextValue(i,0).Trim().Trim(false);
      if (addressString != "" && addressString != "Click here to input the address...")
      {
         count++;
      }
   }
   if (count == 0)
   {
      wxMessageBox(_("Please input at least a valid address"), _("Validation error"), wxOK|wxICON_EXCLAMATION, this);
      return false;
   }
   return true;
}

void CChannelEdit::OnOkClicked( wxCommandEvent& event )
{
   if (ValidateData())
   {
      EndModal(wxID_OK);
   }
}

void CChannelEdit::OnAddAddress( wxCommandEvent& event ) 
{
      wxVector<wxVariant> data;
      data.push_back("Click here to input the address...");
      m_addressList->AppendItem(data);
      m_addressList->SelectRow(m_addressList->GetItemCount() - 1);
}

void CChannelEdit::OnDeleteAddress( wxCommandEvent& event ) 
{
   if (m_addressList->GetSelectedRow() != wxNOT_FOUND)
   {
      if (wxMessageBox(_("Do you really wish to delete the selected address?"),
                           _("Question"), 
                             wxYES_NO|wxICON_QUESTION, NULL) == wxYES)
     {
         m_addressList->DeleteItem(m_addressList->GetSelectedRow());
     }
                               
  }
}

void CChannelEdit::OnUpClicked(wxCommandEvent& event)
{
   if (m_addressList->GetSelectedRow() != wxNOT_FOUND && m_addressList->GetSelectedRow() >= 1)
   {
      int index1 = m_addressList->GetSelectedRow();
      wxString url1 = m_addressList->GetTextValue(index1, 0);
      wxString url2 = m_addressList->GetTextValue(index1 - 1, 0);
      m_addressList->SetTextValue(url2, index1, 0);
      m_addressList->SetTextValue(url1, index1 - 1, 0);
      m_addressList->SelectRow(index1 - 1);
      m_addressList->UnselectRow(index1);
   }   
}

void CChannelEdit::OnDownClicked(wxCommandEvent& event)
{
 if (m_addressList->GetSelectedRow() != wxNOT_FOUND && m_addressList->GetSelectedRow() <= m_addressList->GetItemCount() - 2)
 {
      int index1 = m_addressList->GetSelectedRow();
      wxString url1 = m_addressList->GetTextValue(index1, 0);
      wxString url2 = m_addressList->GetTextValue(index1 + 1, 0);
      m_addressList->SetTextValue(url2, index1, 0);
      m_addressList->SetTextValue(url1, index1 + 1, 0);
      m_addressList->SelectRow(index1 + 1);
      m_addressList->UnselectRow(index1);
   }     
}
