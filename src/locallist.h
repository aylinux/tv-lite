#ifndef CLOCALLIST_H
#define CLOCALLIST_H

#include "subscription.h"
#include "channel.h"

namespace tvlite
{
   
class CLocalList;

WX_DECLARE_OBJARRAY(CLocalList, TLocalLists);      

class CLocalList : public CSubscription
{
private:
   using CSubscription::ReadData; //hide inherited from base
public:
   CLocalList();
   CLocalList(wxString uri);
   int ReadData(); //new function
   virtual ~CLocalList();
   static void GetStoredData(TLocalLists &subList);
   int AddChannel(CChannel channel);
   int UpdateChannel(CChannel channel);
   int DeleteChannel(CChannel channel);
   int UpdateInfo(CSubscriptionInfo &info);
    static int Compare( CLocalList **item1, CLocalList **item2);
protected:
   static void GetCachedData(TSubscriptionList &subList) = delete;
   static wxString FindCachedData(wxString url) = delete;
   int  SaveDataToCache() = delete;
};

}

#endif // CLOCALLIST_H
