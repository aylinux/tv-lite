#ifndef CLOCALLISTDIALOG_H
#define CLOCALLISTDIALOG_H

#ifdef __WXMSW__
#include "gui-windows.h"
#else
#include "gui.h"
#endif

#include "main.h"
#include "locallist.h"

namespace tvlite
{

class CLocalListDialog : public CLocalListDialogBase
{
private:
   CLocalListDialog(const CLocalListDialog& rhs) = delete;
   CLocalListDialog& operator=(const CLocalListDialog& rhs) = delete;
   TLocalLists *m_localLists;


public:
   CLocalListDialog() = delete;
   CLocalListDialog(wxWindow* parent);
   virtual ~CLocalListDialog();
   
   void SetLists(TLocalLists * localLists);
   
   virtual void OnNewClick( wxCommandEvent& event ); 
	virtual void OnEditClick( wxCommandEvent& event ); 
   virtual void OnDeleteClick( wxCommandEvent& event );
   void Repopulate(wxString pathSel = "");

};

}

#endif // CLOCALLISTDIALOG_H
