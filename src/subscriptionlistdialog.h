#ifndef CSUBSCRIPTIONLISTDIALOG_H
#define CSUBSCRIPTIONLISTDIALOG_H

#include <wx/wx.h>
#include <wx/window.h> 
#ifdef __WXMSW__
#include "gui-windows.h"
#else
#include "gui.h"
#endif
#include "main.h"
#include "subscription.h"

namespace tvlite
{ 

class CSubscriptionListDialog : public CSubscriptionListDialogBase
{
private:
   CSubscriptionListDialog(const CSubscriptionListDialog& rhs) = delete;
   CSubscriptionListDialog& operator=(const CSubscriptionListDialog& rhs) = delete;
   TSubscriptionList *m_subscriptionList;

public:
   CSubscriptionListDialog(wxWindow *parent);
   virtual ~CSubscriptionListDialog();
   void Repopulate(wxString pathSel = "");
   virtual void OnNewClicked( wxCommandEvent& event ); 
   virtual void OnEditClicked( wxCommandEvent& event ); 
	virtual void OnDeleteClicked( wxCommandEvent& event ); 
   virtual void OnUpdateButtonClick( wxCommandEvent& event );


};

}
#endif // CSUBSCRIPTIONLISTDIALOG_H
