#ifndef CCHANNELEDIT_H
#define CCHANNELEDIT_H
#ifdef __WXMSW__

#include "gui-windows.h"
#else
#include "gui.h"
#endif

#include "channel.h"

namespace tvlite
{

class CChannelEdit:public CChannelEditBase
{
private:
   CChannelEdit(const CChannelEdit& rhs);
   CChannelEdit& operator=(const CChannelEdit& rhs);
   bool ValidateData();

public:
   CChannelEdit();
   CChannelEdit(wxWindow *parent);
   virtual ~CChannelEdit();
   void GetData(CChannel *channel);
   void SetData(CChannel *channel);
   
protected:
   virtual void OnAddAddress( wxCommandEvent& event ); 
   virtual void OnDeleteAddress( wxCommandEvent& event ); 
   virtual void OnOkClicked( wxCommandEvent& event );
   virtual void OnUpClicked( wxCommandEvent& event );
   virtual void OnDownClicked( wxCommandEvent& event );

};

}

#endif // CCHANNELEDIT_H
