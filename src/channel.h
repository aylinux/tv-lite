#ifndef CCHANNEL_H
#define CCHANNEL_H
#include <string>
#include <vector>
#include <wx/string.h>
#include <wx/dynarray.h>

WX_DECLARE_OBJARRAY(wxString, streamurltype);

namespace tvlite
{

class CChannel
{
public:
   CChannel();
   CChannel(wxString id, wxString name, streamurltype streamurls, wxString iconfile);
   ~CChannel();
   wxString GetName();
   void SetName(wxString name);
   wxString GetId();
   void SetId(wxString id);
   streamurltype GetStreamURLs();
   streamurltype GetAudioChannels();
   void SetStreamURLs(streamurltype);
   void SetAudioChannels(streamurltype);
   static int ConvertFromJSONArray(wxString s, streamurltype&);
   static wxString ConvertToJSONArray(streamurltype& sURLs);
   static wxString ConvertToJSONParam(streamurltype& sURLs);
   static int CompareAsc( CChannel **item1, CChannel **item2);
   static int CompareDesc( CChannel **item1, CChannel **item2);
private:
   wxString m_id;
   wxString m_name;
   streamurltype m_streamurls;
   streamurltype m_audiochannels;
   wxString m_icon;
};

WX_DECLARE_OBJARRAY(CChannel, TChannelList);

}

#endif // CCHANNEL_H
