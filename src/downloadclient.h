#ifndef CDOWNLOADCLIENT_H
#define CDOWNLOADCLIENT_H

#include <wx/wx.h>
#include <wx/event.h>
#include <wx/string.h>
#include <wx/ffile.h>
#include <curl/curl.h>
DECLARE_EVENT_TYPE(DLNOTIFY_EVT, -1)
DECLARE_EVENT_TYPE(DLPROGRESS_EVT, -1)

namespace tvlite
{

struct TDownloadInfo
{
   TDownloadInfo();
   curl_off_t offset;
   curl_off_t size;
   bool have_size;
   bool size_checked;
   
};   
   
class CDownloadClient : public wxThread

{
   public:
   CDownloadClient(wxEvtHandler *handler);
   int InitCurl();
   int Download(wxString url, wxString target);
   void SetEventHandler(wxEvtHandler *evtHandler);
   void Notify(int result);
   bool GetCanceled();
   void SetCanceled(bool canceled);
   ~CDownloadClient();
   wxFFile * GetWriteFile();
   virtual ExitCode Entry();
   bool GetDownloadSizeExists();
   void SetDownloadSizeExists(bool exists);
   bool GetSizeChecked();
   void SetSizeChecked(bool checked);
   curl_off_t GetDownloadSize();
   void SetDownloadSize(curl_off_t size);
   curl_off_t GetDownloadOffset();
   void SetDownloadOffset(curl_off_t offset);
   CURL * GetCurlHandle();

   private:
   CDownloadClient();
   CURL * m_curl_handle;
   wxEvtHandler *m_evtHandler;
   TDownloadInfo m_dlInfo;
   bool m_canceled;
   static int WriteData_cb(void *ptr, size_t size, size_t nmemb, void *stream);
   wxMutex m_accessMutex;
   wxString m_url;
   wxString m_target;
   wxFFile * m_writeFile; //as local variable in the download thread

};

}

#endif // CDOWNLOADCLIENT_H
