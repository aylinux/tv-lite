#include "vlcplayer.h"
#include <wx/window.h>
#include <wx/stdpaths.h>
#include <wx/utils.h>
#include "debug.h"
#include "userdataobject.h"

using namespace tvlite;

CVlcPlayer::CVlcPlayer(wxEvtHandler *parent) :
   m_vlc_inst(NULL),
   m_media_player(NULL),
   m_vlc_evt_man(NULL),
#ifdef __WXGTK__
   m_embedded(NULL),
#endif
#ifdef __WXMSW__
   m_wnd(NULL),
#endif   
   m_wasStopped(false),
   m_rState(rsNone),
   m_pparent(parent)
{
   m_trackMap[etVideo] = -1;
   m_trackMap[etAudio] = -1;
   m_trackMap[etSubtitle] =-1;
}

CVlcPlayer::~CVlcPlayer()
{
   if (m_vlc_inst) libvlc_release(m_vlc_inst);
   if (m_media_player) libvlc_media_player_release(m_media_player);
}


void CVlcPlayer::Init(wxWindow *wnd)
{
    const char* szoptions[3] = {"--network-caching=5000","--live-caching=1000", "--disk-caching=1000"} ;
#ifdef __WXMSW__ 
    wxStandardPaths stdPath = wxStandardPaths::Get();
    wxString sPluginPath = stdPath.GetPluginsDir() + "\\plugins";	
    wxSetEnv("VLC_PLUGIN_PATH",sPluginPath);
    DBG_INFO("plugin path %s", (const char*)sPluginPath.utf8_str());
#endif    
    m_vlc_inst = libvlc_new(2, szoptions);
    m_media_player = libvlc_media_player_new(m_vlc_inst);
    m_vlc_evt_man = libvlc_media_player_event_manager(m_media_player);
    #ifdef __WXGTK__
        GtkWidget *gtk_player_widget = static_cast<GtkWidget *>(wnd->GetHandle());
        gtk_widget_realize(gtk_player_widget);
        m_embedded = gtk_widget_get_window(gtk_player_widget);
        libvlc_media_player_set_xwindow(m_media_player, GDK_WINDOW_XID(m_embedded));
    #else
        libvlc_media_player_set_hwnd(m_media_player, wnd->GetHandle());
        m_wnd = wnd;
    #endif
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaPlayerBuffering,   VlcEvtCallback, this );
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaPlayerPlaying,         VlcEvtCallback, this );
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaPlayerPaused,          VlcEvtCallback, this );
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaPlayerStopped,         VlcEvtCallback, this );
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaPlayerEndReached,      VlcEvtCallback, this );
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaPlayerPositionChanged, VlcEvtCallback, this );
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaPlayerLengthChanged,   VlcEvtCallback, this );
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaPlayerEncounteredError,VlcEvtCallback, this );
  
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaPlayerCorked, VlcEvtCallback, this );
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaPlayerUncorked, VlcEvtCallback, this );
   
    libvlc_event_attach( m_vlc_evt_man, libvlc_VlmMediaInstanceStatusError, VlcEvtCallback, this );
    libvlc_event_attach( m_vlc_evt_man, libvlc_VlmMediaInstanceStopped, VlcEvtCallback, this );
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaPlayerEndReached, VlcEvtCallback, this );
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaPlayerSeekableChanged, VlcEvtCallback, this );
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaParsedChanged, VlcEvtCallback, this );
  
}

void CVlcPlayer::SetEmbedded()
{
        libvlc_set_fullscreen	(m_media_player, false);	
        
}

void CVlcPlayer::SetFull()
{       
        libvlc_set_fullscreen	(m_media_player, true);	
}      

void CVlcPlayer::Play(wxString location)
{   
        DBG_INFO("Entering Play");
        m_wasStopped = false;
        if (location == "")
        {
           return;
        }
        m_location = location;
        #ifdef __WXMSW__
        m_wnd->SetFocus();
        #endif
        //TODO uri disseminate
        libvlc_media_t *media;
        media = libvlc_media_new_location(m_vlc_inst, (const char*)location.utf8_str());
//        libvlc_media_add_option(media,"--network-caching=50000");
//        libvlc_media_add_option(media,"--live-caching=50000");
//        libvlc_media_add_option(media,"--disk-caching=50000");

        libvlc_video_set_mouse_input(m_media_player, FALSE);
        libvlc_media_player_set_media(m_media_player, media);
//for recording        
        if (m_rState == rsFile)
        {
           wxString options = ":sout=#duplicate{dst=display,dst=std{access=file,mux=ts,dst="  + m_recordFileName + "}" ; 
           libvlc_media_add_option(media,(const char*)options.utf8_str());   
        }
//for transmitting
        else if (m_rState == rsStream)
        {   
            libvlc_media_add_option(media,":sout=#duplicate{dst=display,dst=std{access=http,mux=ts,dst=:50100}");
        }
        libvlc_media_player_play(m_media_player);
        libvlc_media_release(media);
}   

void CVlcPlayer::Play()
{ 
   ResetTracks();
   Play(m_location);
}

void CVlcPlayer::Stop()
{
       m_wasStopped = true;
       libvlc_media_player_pause(m_media_player);
       libvlc_media_player_stop (m_media_player);
       ResetTracks();
}

void CVlcPlayer::VlcEvtCallback(const struct libvlc_event_t *p_event, void *p_data)
{
       wxCommandEvent plevent(playerEVT);
       libvlc_event_t *evdata = new libvlc_event_t;
       *evdata = *p_event;
       plevent.SetClientObject((wxClientData*) new UserDataObject((void *)evdata));       
       wxQueueEvent(static_cast<CVlcPlayer *>(p_data)->m_pparent, plevent.Clone());
       if (p_event->type ==libvlc_MediaPlayerPlaying)
       {
         DBG_INFO("Subtitle track is %d from a count of %d" ,libvlc_video_get_spu(static_cast<CVlcPlayer *>(p_data)->m_media_player),
                        libvlc_video_get_spu(static_cast<CVlcPlayer *>(p_data)->m_media_player));
         libvlc_video_set_spu(static_cast<CVlcPlayer *>(p_data)->m_media_player, static_cast<CVlcPlayer *>(p_data)->m_trackMap[etSubtitle]);
                
       }
}

void CVlcPlayer::SetVolume(int value)
{
   libvlc_audio_set_volume(m_media_player,value);
}

void CVlcPlayer::SetAspectRatio(wxString aspectRatio)
{
   if (aspectRatio == "")
   {
       libvlc_video_set_aspect_ratio(m_media_player, NULL);
   }
   else
   {
      libvlc_video_set_aspect_ratio(m_media_player, (const char*)aspectRatio.utf8_str());
   }
}

bool CVlcPlayer::WasStopped()
{
   return m_wasStopped;
}

void CVlcPlayer::Pause()
{
   libvlc_media_player_pause(m_media_player);
}

void CVlcPlayer::Seek(float position)
{
   libvlc_media_player_set_position(m_media_player, position);
}

float CVlcPlayer::GetPosition()
{
   if (m_media_player)
   {
      return libvlc_media_player_get_position(m_media_player);
   }
   else
   {
      return 0.0;
   }
}

void CVlcPlayer::SetRecordState(EVLCRecordState state)
{
   m_rState = state;
}

EVLCRecordState CVlcPlayer::GetRecordState()
{
   return m_rState;
}

void CVlcPlayer::SetRecordFileName(wxString name)
{
   m_recordFileName = name;
}

wxString CVlcPlayer::GetRecordFileName()
{
   return m_recordFileName;
}

#include <wx/arrimpl.cpp>
WX_DEFINE_OBJARRAY(TTrackInfoList);

int CVlcPlayer::GetTracks(TTrackInfoList &tracklist, ETrackType trackType)
{
   libvlc_track_description_t *p_tracks;
   
   switch (trackType)
   {
   case etVideo:
      p_tracks = libvlc_video_get_track_description(m_media_player);
      break;
   case  etAudio:
      p_tracks = libvlc_audio_get_track_description(m_media_player);
      break;
   case  etSubtitle:
      p_tracks = libvlc_video_get_spu_description(m_media_player);
      break;   
    default:
      return -1;
      break;
   }
   libvlc_track_description_t * iter = p_tracks;
   while (iter)
   {
        CTrackInfo crt_track;
        crt_track.SetType(trackType);
        wxString info = iter->psz_name;
        if (info == "")
        {
             info << "Track " << iter->i_id;
        }
        crt_track.SetInfo(info);
        crt_track.SetIndex(iter->i_id);
        tracklist.Add(crt_track);
        iter = iter->p_next;
   }  
   for (size_t i = 0;  i < tracklist.Count(); i++)
   {
       DBG_INFO("track %ld, index = %d, type = %d, info = %s", i, 
               tracklist[i].GetIndex(), 
               tracklist[i].GetType(),
               (const char*)tracklist[i].GetInfo().utf8_str());
   }  
   libvlc_track_description_list_release(p_tracks);
   return 0;
}



void  CVlcPlayer::SetTrack(int trackid, ETrackType trackType)
{
   int result;
   if (trackType == etSubtitle)
   {
      DBG_INFO("setting subtitle track, %d", trackid);
      result = libvlc_video_set_spu(m_media_player, trackid);
      DBG_INFO("result = %d", result);
      m_trackMap[etSubtitle] = trackid;
   }
   if (trackType == etAudio)
   {
      DBG_INFO("setting audio track, %d", trackid);
      result = libvlc_audio_set_track(m_media_player,trackid);
      DBG_INFO("result = %d", result);
      m_trackMap[etAudio] = trackid;
   }
   if (trackType == etVideo)
   {
      DBG_INFO("setting video track, %d", trackid);
      result = libvlc_video_set_track(m_media_player,trackid);
      DBG_INFO("result = %d", result);
      m_trackMap[etVideo] = trackid;
   }
}

void CVlcPlayer::RestoreTracks()
{
   int result;
   DBG_INFO("Restoring tracks");
   if (m_trackMap[etSubtitle] != -1)
   {
      DBG_INFO("Subtitle track %d ", m_trackMap[etSubtitle]);
      result = libvlc_video_set_spu(m_media_player, m_trackMap[etSubtitle]);
      DBG_INFO("result = %d", result);
   }
   if (m_trackMap[etAudio] != -1)
   {
      DBG_INFO("Audio track %d", m_trackMap[etAudio]);
      result = libvlc_audio_set_track(m_media_player, m_trackMap[etAudio]);
      DBG_INFO("result = %d", result);
   }
   if (m_trackMap[etVideo] != -1)
   {
      DBG_INFO("Video track, %d", m_trackMap[etVideo]);
      result = libvlc_video_set_track(m_media_player, m_trackMap[etVideo]);
      DBG_INFO("result = %d", result);
   }
   
}   

void CVlcPlayer::ResetTracks()
{
   m_trackMap[etVideo] = -1;
   m_trackMap[etAudio] = -1;
   m_trackMap[etSubtitle] =-1;
}