#ifndef CCONFIGURATION_H
#define CCONFIGURATION_H

#include <wx/string.h>

namespace tvlite
{

struct TConfiguration
{
   wxString dummy;
};   
   
class CConfiguration
{
   static CConfiguration* ms_instance;
   const  wxString configName = "tvlite";
  
private:
   CConfiguration(const CConfiguration& rhs);
   CConfiguration& operator=(const CConfiguration& rhs);
   bool Init(wxString dir);

public:
   static CConfiguration* Instance();
   bool Init();
   static void Release();
   static wxString GetConfigDir();
   static wxString GetConfigFileName();
   static wxString GetTempDir();
   static wxString GetCacheDir();
   static wxString GetListDir();
   wxString GetRecordDir();
   void SetRecordDir(wxString path);

private:
   CConfiguration();
   ~CConfiguration();
   TConfiguration Config;
   wxString m_recordDir;

};

}

#endif // CCONFIGURATION_H
