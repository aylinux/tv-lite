#ifndef CSTREAMPROTOCOLHANDLER_H
#define CSTREAMPROTOCOLHANDLER_H

#include "baseprotocolhandler.h"

namespace tvlite
{

class CStreamProtocolHandler : public CBaseProtocolHandler
{
private:
   CStreamProtocolHandler(const CStreamProtocolHandler& rhs) = delete;
   CStreamProtocolHandler& operator=(const CStreamProtocolHandler& rhs) = delete;
   
public:
   CStreamProtocolHandler() = delete;
   CStreamProtocolHandler(wxEvtHandler *parent, wxString url);
   ~CStreamProtocolHandler();
   virtual void Start() ;
   virtual void Stop() ;

};

}

#endif // CSOPPROTOCOLHANDLER_H
