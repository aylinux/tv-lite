#include <wx/wx.h>
#include "subscription.h"
#include "subscriptiondialog.h"
#include "subscriptionlistdialog.h"
#include "debug.h"

using namespace tvlite;

CSubscriptionListDialog::CSubscriptionListDialog(wxWindow* parent):CSubscriptionListDialogBase(parent)
{
   int width, height;
   m_SubscriptionDataList->GetSize(&width, &height);
   m_SubscriptionDataList->AppendTextColumn( _("Name"), wxDATAVIEW_CELL_INERT, width/3 );
   m_SubscriptionDataList->AppendTextColumn( _("URL"), wxDATAVIEW_CELL_INERT,width*2/3 );
   m_subscriptionList = ((MainFrame*)wxGetApp().GetTopWindow())->GetSubscriptionList();
   Repopulate();
}

CSubscriptionListDialog::~CSubscriptionListDialog()
{
}

void CSubscriptionListDialog::OnNewClicked( wxCommandEvent& event )
{
   CSubscriptionDialog *newDialog = new CSubscriptionDialog(this);
   newDialog->ShowURLOnly();
   newDialog->Destroy(); 
   delete newDialog;
  
} 

void CSubscriptionListDialog::OnEditClicked( wxCommandEvent& event ) 
{
   CSubscriptionDialog *editDialog = new CSubscriptionDialog(this);
   int index = m_SubscriptionDataList->GetSelectedRow();
   if (index != wxNOT_FOUND)
   {
      CSubscription &sub = (*m_subscriptionList)[index];
      editDialog->SetNameField(sub.GetSubscriptionInfo().name);
      editDialog->SetURLField (sub.GetSubscriptionInfo().url);
      editDialog->ShowModal();
   }
   editDialog->Destroy();
   delete editDialog;
}


void CSubscriptionListDialog::OnUpdateButtonClick( wxCommandEvent& event )
{
   
   int index = m_SubscriptionDataList->GetSelectedRow();
   if (index != wxNOT_FOUND)
   {
      CSubscription &sub = (*m_subscriptionList)[index];
      wxString deleteFileName = CSubscription::FindCachedData(sub.GetSubscriptionInfo().url);
      CSubscription newSub(sub.GetSubscriptionInfo().url);
      CSubscriptionInfo &subInfo = newSub.GetSubscriptionInfo();
      subInfo.name = sub.GetSubscriptionInfo().name ;
      if (newSub.ReadData())
      {
         wxMessageBox(_("Could not update the selected subscription"), _("Error"), wxOK | wxICON_ERROR, NULL);
         return;
      }
      wxString path;
      newSub.SaveDataToCache(path);
         
      if (deleteFileName != "")
      {
         wxRemoveFile(deleteFileName);
      }
      ((MainFrame*)wxGetApp().GetTopWindow())->RefreshInterface(path);
      Repopulate(path);
      wxMessageBox(_("Subscription Updated"), _("Update"), wxOK | wxICON_INFORMATION, NULL);
   }
}

void CSubscriptionListDialog::OnDeleteClicked( wxCommandEvent& event ) 
{
    int index = m_SubscriptionDataList->GetSelectedRow();
    if (index != wxNOT_FOUND)
    {
       CSubscription &sub = (*m_subscriptionList)[index];
       wxString deleteFileName = CSubscription::FindCachedData(sub.GetSubscriptionInfo().url);
       if (deleteFileName != "")
       {
          if (wxMessageBox(_("Do you really wish to delete the selected subscription, ") + sub.GetSubscriptionInfo().name +"?",
                           _("Question"), 
                             wxYES_NO|wxICON_QUESTION, NULL) == wxYES)
          {
            wxRemoveFile(deleteFileName);
           
            ((MainFrame*)wxGetApp().GetTopWindow())->RefreshInterface();
             Repopulate();
             
          }
       }
    }
       
}

void CSubscriptionListDialog::Repopulate(wxString pathSel)
{
   unsigned int listCount;
   unsigned int index = 0;
   DBG_INFO("pathSel is: %s", (const char*)pathSel.utf8_str());
   //CSubscription::GetCachedData(*m_subscriptionList);
   listCount=m_subscriptionList->GetCount();
   m_SubscriptionDataList->DeleteAllItems();
   for (unsigned int i = 0; i < listCount; i++)
   {
      wxVector<wxVariant>data;
      data.push_back((*m_subscriptionList)[i].GetSubscriptionInfo().name);
      data.push_back((*m_subscriptionList)[i].GetSubscriptionInfo().url);
      m_SubscriptionDataList->AppendItem(data);
      if ((*m_subscriptionList)[i].GetURI() == "file://" + pathSel)
      {
         index = i;
      }
         
      data.clear();
   }
   m_SubscriptionDataList->SelectRow(index);
}