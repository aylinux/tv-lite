///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/string.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/menu.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/radiobox.h>
#include <wx/stattext.h>
#include <wx/choice.h>
#include <wx/listctrl.h>
#include <wx/statbmp.h>
#include <wx/textctrl.h>
#include <wx/sizer.h>
#include <wx/panel.h>
#include <wx/bmpbuttn.h>
#include <wx/button.h>
#include <wx/slider.h>
#include <wx/splitter.h>
#include <wx/timer.h>
#include <wx/statusbr.h>
#include <wx/frame.h>
#include <wx/dialog.h>
#include <wx/dataview.h>
#include <wx/gauge.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class MainFrameBase
///////////////////////////////////////////////////////////////////////////////
class MainFrameBase : public wxFrame
{
	private:

	protected:
		wxMenuBar* m_menuBar;
		wxMenu* m_menuFile;
		wxMenu* m_menuManage;
		wxMenuItem* m_menuLocalLists;
		wxMenuItem* m_menuSubscriptions;
		wxMenu* m_menuView;
		wxMenuItem* m_menuChannelList;
		wxMenu* m_videoTracks;
		wxMenu* m_audioTracks;
		wxMenu* m_subtitleTracks;
		wxMenu* m_helpMenu;
		wxSplitterWindow* m_splitter1;
		wxPanel* m_panel3;
		wxRadioBox* m_listType;
		wxStaticText* m_staticText1;
		wxChoice* m_choice1;
		wxStaticText* m_statistics;
		wxStaticBitmap* m_bitmap2;
		wxTextCtrl* m_channelSearch;
		wxRadioBox* m_searchOrder;
		wxPanel* m_panel4;
		wxBoxSizer* bSizer3;
		wxPanel* m_tvView;
		wxBoxSizer* bSizer41;
		wxBitmapButton* m_bpButtonPlay;
		wxBitmapButton* m_bpButtonStop;
		wxBitmapButton* m_bpButtonPause;
		wxBitmapButton* m_bpButtonRecord;
		wxSlider* m_movieslider;
		wxStaticBitmap* m_bitmap1;
		wxSlider* m_slider1;
		wxBitmapButton* m_bpButtonFullScreen;
		wxTimer m_trtimer;
		wxStatusBar* m_statusBar;

		// Virtual event handlers, overide them in your derived class
		virtual void OnCloseFrame( wxCloseEvent& event ) { event.Skip(); }
		virtual void OnMaximizeFrame( wxMaximizeEvent& event ) { event.Skip(); }
		virtual void OnMove( wxMoveEvent& event ) { event.Skip(); }
		virtual void OnPaintEvent( wxPaintEvent& event ) { event.Skip(); }
		virtual void OnShowFrame( wxShowEvent& event ) { event.Skip(); }
		virtual void OnSize( wxSizeEvent& event ) { event.Skip(); }
		virtual void OnLocalListAdd( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSaveList( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSaveView( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSaveSubscriptionAsLocalList( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnLocalListClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSubscriptionClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnExitClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnFullScreen( wxCommandEvent& event ) { event.Skip(); }
		virtual void ToggleChannelList( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnARDefault( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnAR169( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnAR43( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnAbout( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnListTypeClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSubscriptionSelected( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnListPlay( wxListEvent& event ) { event.Skip(); }
		virtual void OnListContextMenu( wxListEvent& event ) { event.Skip(); }
		virtual void OnListContextMenu( wxMouseEvent& event ) { event.Skip(); }
		virtual void OnPopulateChannel( wxCommandEvent& event ) { event.Skip(); }
		virtual void KeyPressed( wxKeyEvent& event ) { event.Skip(); }
		virtual void ToggleFullScreen( wxMouseEvent& event ) { event.Skip(); }
		virtual void OnPlay( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnStop( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnPause( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnRecord( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSeekMouseChanged( wxMouseEvent& event ) { event.Skip(); }
		virtual void OnSeekUsrChanged( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnVolumeChanged( wxScrollEvent& event ) { event.Skip(); }
		virtual void OnVolumeChanged( wxCommandEvent& event ) { event.Skip(); }
		virtual void HideBar( wxTimerEvent& event ) { event.Skip(); }


	public:
		wxListCtrl* m_ChannelList;

		MainFrameBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("TV-Lite"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 1024,600 ), long style = wxCLOSE_BOX|wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );

		~MainFrameBase();

		void m_splitter1OnIdle( wxIdleEvent& )
		{
			m_splitter1->SetSashPosition( 300 );
			m_splitter1->Disconnect( wxEVT_IDLE, wxIdleEventHandler( MainFrameBase::m_splitter1OnIdle ), NULL, this );
		}

};

///////////////////////////////////////////////////////////////////////////////
/// Class CLocalListDialogBase
///////////////////////////////////////////////////////////////////////////////
class CLocalListDialogBase : public wxDialog
{
	private:

	protected:
		wxListCtrl* m_LocalDataList;
		wxButton* m_newButton;
		wxButton* m_editButton;
		wxButton* m_deleteButton;
		wxButton* m_closeButton;

		// Virtual event handlers, overide them in your derived class
		virtual void OnNewClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnEditClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnDeleteClick( wxCommandEvent& event ) { event.Skip(); }


	public:

		CLocalListDialogBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Local Lists"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,-1 ), long style = wxDEFAULT_DIALOG_STYLE );
		~CLocalListDialogBase();

};

///////////////////////////////////////////////////////////////////////////////
/// Class CSubscriptionListDialogBase
///////////////////////////////////////////////////////////////////////////////
class CSubscriptionListDialogBase : public wxDialog
{
	private:

	protected:
		wxDataViewListCtrl* m_SubscriptionDataList;
		wxButton* m_newButton;
		wxButton* m_updateButton;
		wxButton* m_editButton;
		wxButton* m_deleteButton;
		wxButton* m_close;

		// Virtual event handlers, overide them in your derived class
		virtual void OnNewClicked( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnUpdateButtonClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnEditClicked( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnDeleteClicked( wxCommandEvent& event ) { event.Skip(); }


	public:

		CSubscriptionListDialogBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Subscriptions"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,-1 ), long style = wxDEFAULT_DIALOG_STYLE );
		~CSubscriptionListDialogBase();

};

///////////////////////////////////////////////////////////////////////////////
/// Class CSubscriptionDialogBase
///////////////////////////////////////////////////////////////////////////////
class CSubscriptionDialogBase : public wxDialog
{
	private:

	protected:
		wxStaticText* m_NameLabel;
		wxTextCtrl* m_Name;
		wxStaticText* m_staticText2;
		wxTextCtrl* m_URLText;
		wxButton* m_PathButton;
		wxStdDialogButtonSizer* m_sdbSizer2;
		wxButton* m_sdbSizer2OK;
		wxButton* m_sdbSizer2Cancel;

		// Virtual event handlers, overide them in your derived class
		virtual void OnBrowseButtonClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnCancelButtonClicked( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnOkButtonClicked( wxCommandEvent& event ) { event.Skip(); }


	public:

		CSubscriptionDialogBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Path"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 766,-1 ), long style = wxDEFAULT_DIALOG_STYLE|wxRESIZE_BORDER );
		~CSubscriptionDialogBase();

};

///////////////////////////////////////////////////////////////////////////////
/// Class CLocalDialogBase
///////////////////////////////////////////////////////////////////////////////
class CLocalDialogBase : public wxDialog
{
	private:

	protected:
		wxStaticText* m_NameLabel;
		wxTextCtrl* m_Name;
		wxStaticText* m_AuthorLabel;
		wxTextCtrl* m_Author;
		wxStaticText* m_URLLabel;
		wxTextCtrl* m_urlText;
		wxStaticText* m_VersionLabel;
		wxTextCtrl* m_versionText;
		wxStdDialogButtonSizer* m_sdbSizer2;
		wxButton* m_sdbSizer2OK;
		wxButton* m_sdbSizer2Cancel;

		// Virtual event handlers, overide them in your derived class
		virtual void OnOkClicked( wxCommandEvent& event ) { event.Skip(); }


	public:

		CLocalDialogBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Edit Local List"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,-1 ), long style = wxDEFAULT_DIALOG_STYLE|wxRESIZE_BORDER );
		~CLocalDialogBase();

};

///////////////////////////////////////////////////////////////////////////////
/// Class CChannelEditBase
///////////////////////////////////////////////////////////////////////////////
class CChannelEditBase : public wxDialog
{
	private:

	protected:
		wxStaticText* m_staticText8;
		wxTextCtrl* m_channelName;
		wxStaticText* m_staticText9;
		wxDataViewListCtrl* m_addressList;
		wxBitmapButton* m_addButton;
		wxBitmapButton* m_deleteButton;
		wxBitmapButton* m_upButton;
		wxBitmapButton* m_downButton;
		wxStdDialogButtonSizer* m_sdbSizer3;
		wxButton* m_sdbSizer3OK;
		wxButton* m_sdbSizer3Cancel;

		// Virtual event handlers, overide them in your derived class
		virtual void OnAddAddress( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnDeleteAddress( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnUpClicked( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnDownClicked( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnOkClicked( wxCommandEvent& event ) { event.Skip(); }


	public:

		CChannelEditBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE );
		~CChannelEditBase();

};

///////////////////////////////////////////////////////////////////////////////
/// Class CDlProgressBase
///////////////////////////////////////////////////////////////////////////////
class CDlProgressBase : public wxDialog
{
	private:

	protected:
		wxStaticText* m_urlText;
		wxGauge* m_progressGauge;
		wxStdDialogButtonSizer* m_sdbSizer4;
		wxButton* m_sdbSizer4Cancel;

		// Virtual event handlers, overide them in your derived class
		virtual void Init( wxInitDialogEvent& event ) { event.Skip(); }
		virtual void OnCancelClicked( wxCommandEvent& event ) { event.Skip(); }


	public:

		CDlProgressBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,-1 ), long style = wxDEFAULT_DIALOG_STYLE );
		~CDlProgressBase();

};

///////////////////////////////////////////////////////////////////////////////
/// Class CRecordChoiceBase
///////////////////////////////////////////////////////////////////////////////
class CRecordChoiceBase : public wxDialog
{
	private:

	protected:
		wxRadioBox* m_recordChoiceBox;

		// Virtual event handlers, overide them in your derived class
		virtual void OnClickRadioBox( wxCommandEvent& event ) { event.Skip(); }


	public:

		CRecordChoiceBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 140,133 ), long style = wxCAPTION|wxCLOSE_BOX|wxSTAY_ON_TOP );
		~CRecordChoiceBase();

};

