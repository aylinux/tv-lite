#include "streamprotocolhandler.h"
#include "debug.h"

using namespace tvlite;

CStreamProtocolHandler::CStreamProtocolHandler(wxEvtHandler *parent, wxString url) : 
  CBaseProtocolHandler(parent, url)
 
{   

}

CStreamProtocolHandler::~CStreamProtocolHandler()
{
   DBG_INFO("SOP Protocol Handler -> delete")
}

void CStreamProtocolHandler::Start()
{
   m_vlcPlayer->Play(m_url);
   
}

void CStreamProtocolHandler::Stop()
{
   m_vlcPlayer->Stop();
   m_stopped = true;
   OnStopAsync();
}
