#ifndef CLOCALDIALOG_H
#define CLOCALDIALOG_H

#include <wx/frame.h>
#ifdef __WXMSW__
#include "gui-windows.h"
#else
#include "gui.h"
#endif
#include "subscriptioninfo.h"

namespace tvlite
{

class CLocalDialog : public CLocalDialogBase
{
private:
   CLocalDialog(const CLocalDialog& rhs);
   CLocalDialog& operator=(const CLocalDialog& rhs);

public:
   CLocalDialog() = delete;
   CLocalDialog(wxWindow *parent);
   void SetData(CSubscriptionInfo &info);
   void GetData(CSubscriptionInfo &info);
   bool ValidateData();
   virtual ~CLocalDialog();
protected:
   virtual void OnOkClicked( wxCommandEvent& event);

};

}

#endif // CLOCALDIALOG_H
