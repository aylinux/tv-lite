#ifndef CSOPPROTOCOLHANDLER_H
#define CSOPPROTOCOLHANDLER_H
#include <wx/timer.h>
#include "baseprotocolhandler.h"
#include "sopprotocol.h"

DECLARE_EVENT_TYPE(thrEVT_EXIT, -1)

namespace tvlite
{

class CSopProtocolHandler : public CBaseProtocolHandler
{
private:
   CSopProtocolHandler(const CSopProtocolHandler& rhs) = delete;
   CSopProtocolHandler& operator=(const CSopProtocolHandler& rhs) = delete;
   CSopProtocol *m_process;
   CSopThread *m_sopthread;
   long m_protopid;
   wxTimer m_timer;
   
public:
   CSopProtocolHandler() = delete;
   CSopProtocolHandler(wxEvtHandler *parent, wxString url);
   ~CSopProtocolHandler();
   virtual void Start() ;
   virtual void Stop() ;
   CSopThread *GetThread() { return m_sopthread; };
   int LaunchSopProcess();
   void OnExitSOPThread(wxCommandEvent &event);
   void OnExitSOPProcess(wxProcessEvent &event);
   void PlayVLC(wxCommandEvent &event);
   void OnTimerNotify(wxTimerEvent &event);
   

};

}

#endif // CSOPPROTOCOLHANDLER_H
