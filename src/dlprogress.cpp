#include "dlprogress.h"
#include "debug.h"
#include "main.h"

#define E_DLP_ERROR -1
#define E_DLP_OK     0
#define E_DLP_CANCELED -2

#define ID_REFRESH 1000

using namespace tvlite;


CDLProgress::CDLProgress(wxString url, wxString file) :
               CDlProgressBase(wxGetApp().GetTopWindow()), 
               m_url(url),
               m_importfile(file),
               m_timer(this, ID_REFRESH)
{
   Connect(wxID_ANY, DLNOTIFY_EVT, wxThreadEventHandler(CDLProgress::OnDownloadFinished));
   m_dlClient = new CDownloadClient(this);
   m_progressGauge->SetRange(100);
   m_progressGauge->Pulse();
   m_urlText->SetLabel(m_url);
   m_timer.Start(100);
}

CDLProgress::~CDLProgress()
{
   m_timer.Stop();
}

int CDLProgress::InitDlClient()
{
   
   int rc = E_DLP_OK;
   DBG_INFO("InitDlClient called")
   if (m_dlClient->InitCurl())
   {
      rc =  E_DLP_ERROR;
   }
   if (rc == E_DLP_OK)  
   {
      if (m_dlClient->Download(m_url, m_importfile))
      {
          wxMessageBox(_("Error downloading channel list"),_("Error"), wxOK|wxCENTER|wxICON_ERROR);
          rc =  E_DLP_ERROR;
      }   
   }
   Connect( ID_REFRESH, wxEVT_TIMER, wxTimerEventHandler(CDLProgress::onTimer) );
   rc = ShowModal();
   return rc;
}


void CDLProgress::OnCancelClicked( wxCommandEvent& event )
{
   m_dlClient->SetCanceled(true);
   Disconnect( wxEVT_TIMER, wxTimerEventHandler(CDLProgress::onTimer) );
   EndModal(E_DLP_CANCELED);
}

void CDLProgress::OnDownloadFinished( wxThreadEvent& event )
{
   Disconnect( wxEVT_TIMER, wxTimerEventHandler(CDLProgress::onTimer) );
   EndModal(event.GetInt());
}

void CDLProgress::onTimer(wxTimerEvent &event)
{

   if (m_dlClient->GetDownloadSizeExists())
   {
      int val =(int)(m_dlClient->GetDownloadOffset()*100.0/m_dlClient->GetDownloadSize());
      //DBG_INFO("Value is %d", val);
      m_progressGauge->SetValue(val);
   }
   else 
   {
      //DBG_INFO("PULSE")
      m_progressGauge->Pulse();
   }   
   
} 


