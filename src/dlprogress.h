#ifndef CDLPROGRESS_H
#define CDLPROGRESS_H
#include <wx/frame.h>
#ifdef __WXMSW__
#include "gui-windows.h"
#else
#include "gui.h"
#endif
#include "downloadclient.h"
#include <wx/timer.h>

namespace tvlite
{
   
class CDLProgress : public CDlProgressBase
{
private:
   CDLProgress(const CDLProgress& rhs);
   CDLProgress& operator=(const CDLProgress& rhs);
   CDownloadClient *m_dlClient;
   wxString m_url;
   wxString m_importfile;
   wxTimer m_timer;

public:
   CDLProgress() = delete;
   CDLProgress(wxString url, wxString file);
   ~CDLProgress();
   int InitDlClient();
	virtual void OnCancelClicked( wxCommandEvent& event );
   void OnDownloadFinished(wxThreadEvent& event);
   void onTimer(wxTimerEvent &event);

};

}

#endif // CDLPROGRESS_H
