#ifndef CACEPROTOCOL_H
#define CACEPROTOCOL_H

#include "baseprotocol.h"

namespace tvlite
{

class CAceProtocol : public CBaseProtocol
{
private:
   CAceProtocol(const CAceProtocol& rhs);
   CAceProtocol& operator=(const CAceProtocol& rhs);
   CAceProtocol();
   int SearchAce(wxString &cmd);
   bool m_IsInstalled;

public:
   CAceProtocol(wxEvtHandler *parent, wxString url = "", wxString cmd = "");
   virtual ~CAceProtocol();
   virtual int  StartProtocol(long &pid);
   virtual void StopProtocol();
   //virtual void OnTerminate(int pid, int status);
   int LoadConfig();
   int SaveConfig();
  
   wxString GetUrl();
   void SetURL(wxString url);
   bool IsInstalled()
   {
      return m_IsInstalled;
   }
  
   

};

}

#endif // CACEPROTOCOL_H
