#ifndef CSUBSCRIPTIONDIALOG_H
#define CSUBSCRIPTIONDIALOG_H

#include <wx/wx.h>
#include <wx/window.h> 

#ifdef __WXMSW__
#include "gui-windows.h"
#else
#include "gui.h"
#endif

#include "main.h"
#include "subscription.h"

class CSubscriptionDialog : public CSubscriptionDialogBase
{
private:
   CSubscriptionDialog(const CSubscriptionDialog& rhs) = delete;
   CSubscriptionDialog& operator=(const CSubscriptionDialog& rhs) = delete;
   wxString m_subscriptionName;
   wxString m_subscriptionURL;

public:
   CSubscriptionDialog() = delete;
   CSubscriptionDialog(wxWindow *parent);
   void SetNameField(wxString param);
   const wxString GetNameField() const;
   void SetURLField(wxString param);
   const wxString GetURLField() const;
   virtual int ShowModal();
   int ShowURLOnly();
   virtual void OnCancelButtonClicked( wxCommandEvent& event );
   virtual void OnOkButtonClicked( wxCommandEvent& event ); 
   virtual void OnBrowseButtonClick(wxCommandEvent& event);
   virtual ~CSubscriptionDialog();

};

#endif // CSUBSCRIPTIONDIALOG_H
