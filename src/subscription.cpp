#include "downloadclient.h"
#include <wx/filename.h>
#include <wx/dir.h>
#include "debug.h"
#include <wx/wfstream.h>
#include <wx/txtstrm.h>
#include "subscription.h"
#include "subscriptioninfo.h"
#include "configuration.h"
#include "dlprogress.h"
#include "channel.h"
#include "main.h"

#include <string.h>


#define E_SUB_OK      0
#define E_SUB_ERROR -1

using namespace tvlite;

CSubscription::CSubscription()
{
}


CSubscription::CSubscription(wxString uri) : m_uri(uri)
{
}

CSubscription::~CSubscription()
{
}


#include <wx/arrimpl.cpp>
WX_DEFINE_OBJARRAY(TChannelList);


TChannelList CSubscription::GetChannelList(wxString searchString /*= ""*/, bool sorted /*= false */, bool asc /*= true*/)
{
   TChannelList searchlist;
   if (searchString != "")
   {
      
      for (size_t i = 0; i < m_chanlist.Count(); i++)
      {
         if (m_chanlist[i].GetName().Lower().Find(searchString.Lower()) != wxNOT_FOUND)
         {
            searchlist.Add(m_chanlist[i]);
         }
      }
   }
   else
   {
      searchlist = m_chanlist;
   }
   if (sorted)
   {
      if (asc)
      {
         searchlist.Sort(CChannel::CompareAsc);
      }
      else
      {
         searchlist.Sort(CChannel::CompareDesc);
      }
   }   
   return searchlist; 
}



void CSubscription::SetChannelList(TChannelList chanlist)
{
   m_chanlist = chanlist;
}

CSubscriptionInfo& CSubscription::GetSubscriptionInfo()
{
   return m_subscriptionInfo;
}

void CSubscription::SetSubscriptionInfo(CSubscriptionInfo subscriptionInfo)
{
   m_subscriptionInfo = subscriptionInfo;
}

wxString CSubscription::GetURI()
{
   return m_uri;
}

int CSubscription::ReadData(bool fromCache)
{
   wxURI uri;
   wxString readuri;
   wxString importfile;
   int rc = E_SUB_OK;

   if (fromCache)
   {
      readuri = FindCachedData(m_uri);
      DBG_INFO("Reading from cache");
   }
   if (readuri == "")
   {
      readuri = m_uri;
   }      
   if (uri.Create(readuri) != true)
   {
      DBG_ERROR("URI not recognized!");
      rc =  E_DB_CREATE;
   }
   if (rc == E_SUB_OK)
   {
      wxString scheme = uri.GetScheme();
      DBG_INFO("scheme is %s", (const char*)scheme.utf8_str());
      if (scheme == "file" || scheme.length() <= 1)
      {
         DBG_INFO("FileName entered! ")
         
         importfile = wxURI::Unescape(uri.GetPath());
         
      }
      else if (scheme == "http" || scheme == "https") 
      {
         importfile = GenerateTempFileName();
         CDLProgress dlProgress(readuri,importfile);
         rc = dlProgress.InitDlClient();
      }
   }
   if (rc == E_SUB_OK)
   {
      DBG_INFO("Parse from %s",(const char*)importfile.utf8_str());
      rc = ParseData(importfile);
   }
   else
   {
      wxMessageBox(_("Error downloading channel list"),_("Error"), wxOK|wxCENTER|wxICON_ERROR);
   }
   return rc;
}


int CSubscription::ParseData(wxString dbFile)
{
   const unsigned int someLength = 16;
   wxFileInputStream* stream = new wxFileInputStream(dbFile);
   if (stream == NULL || !stream->IsOk())
   {
      DBG_ERROR("File could not be opened");
      return -1;
   }
   unsigned char buffer[someLength];
   memset (buffer, 0, someLength);
   wxInputStream &current = stream->Read((void*)buffer,someLength - 1);
   size_t readBytes = current.LastRead();
   if (readBytes < someLength - 1)
   {
      DBG_ERROR("File too small!")
      delete stream;
      return -1;
   }
   delete stream;
   
   if (!strncmp((const char*)buffer,"SQLite", strlen("SQLite")))
   {
      return ParseDbData(dbFile);
   }
   else if (!strncmp((const char*)buffer,"#EXT", strlen("#EXT")))
   {
      return ParseM3UData(dbFile);
   }
   else 
   {
      wxMessageBox(_("Format not recognized"), _("Error"), wxOK|wxCENTER|wxICON_ERROR);
      DBG_ERROR("Format not recognized");
      return -1;
   }
}   

int CSubscription::ParseDbData(wxString dbFile)  
{
   DBG_INFO("Parsing db data");
   CDataBase db(dbFile);
   int rc;
   rc = db.Init();
   wxString currentName = m_subscriptionInfo.name;
   if (rc == E_DB_OK)
   {
      rc = db.GetInfoTableData(m_subscriptionInfo);
      if (currentName != "")
      {
         m_subscriptionInfo.name = currentName;
      }
   }
   if (rc == E_DB_OK)
   {
      rc = db.GetTVTableData(m_chanlist);
   } 
   return 0;
}

int CSubscription::ParseM3UData(wxString dbFile)
{
   DBG_INFO("Parsing m3u data");
   wxFileName dbFileName(dbFile);
   if (m_subscriptionInfo.name == "")
   {
      m_subscriptionInfo.name = m_uri;
   }
   wxFileInputStream* stream = new wxFileInputStream(dbFile);
   wxTextInputStream text(*stream," \t", wxConvUTF8);
   wxString rest;
   CChannel item;
   while(!stream->Eof())
   {
      wxString line = text.ReadLine();
      line.Trim(false);
      line.Trim();
      if (line == "")
      {
         continue;
      }
      if (line.StartsWith("#EXTINF:", &rest))
      {
         wxString name(rest.AfterLast(','));
         item.SetName(name);
         wxString id = name.BeforeFirst(' ');
         wxString randVal;
         randVal.Printf("%3d", rand() % 1000);
         item.SetId(id + randVal);
      }
      else if (line[0] != '#')
      {
         streamurltype s;
         s.Add(line);
         item.SetStreamURLs(s);
         m_chanlist.Add(item);
      }
   }
   return 0;
} 

wxString CSubscription::GenUUID()
{
#ifdef __WXGTK__    
    uuid_t guid;
    uuid_string_t uuidOut;
    uuid_generate(guid);
    uuid_unparse(guid, (char *)uuidOut);
    wxString uuidStr(uuidOut);
    wxString dash("-");
    wxString none;
    uuidStr.Replace(dash,none);
    return uuidStr;
#else
    Uuid *u = new Uuid();
    u->Create();
    UUID* s_uuid = (UUID*)u;
    wxString uuidStr; // In string form
    uuidStr.Printf( wxT("%08lX%04X%04X%02X%02X%02X%02X%02X%02X%02X%02X"),s_uuid->Data1, s_uuid->Data2, s_uuid->Data3,s_uuid->Data4[0], s_uuid->Data4[1], s_uuid->Data4[2], s_uuid->Data4[3], s_uuid->Data4[4], s_uuid->Data4[5], s_uuid->Data4[6],s_uuid->Data4[7]);
    delete u;
    return uuidStr;
#endif    
    
}
int  CSubscription::SaveDataToCache()
{
   wxString path;
   return SaveDataToCache(path); 
}

int  CSubscription::SaveDataToCache(wxString &idStr)
{
   int rc = E_SUB_OK;
   wxString uuidStr = GenUUID();
   wxString path = wxFileName(CConfiguration::GetCacheDir(), uuidStr).GetFullPath();
   rc = SaveData(path);
   if (rc == E_SUB_OK)
   {
      idStr = path;
   }
   return rc;
}

int  CSubscription::SaveDataToListDir()
{   
   wxString path;
   return SaveDataToListDir(path); 
}   

int  CSubscription::SaveDataToListDir(wxString &idStr)
{
   int rc = E_SUB_OK;
   wxString uuidStr = GenUUID();
   wxString path = wxFileName(CConfiguration::GetListDir(), uuidStr).GetFullPath();
   rc = SaveData(path, true);
   if (rc == E_SUB_OK)
   {
      idStr = path;
   }
   return rc;
}


int  CSubscription::SaveData(wxString path, bool keepuri /* = false */)
{
    int rc = E_SUB_OK;
    CDataBase subDb(path);
    if (subDb.Init() != E_DB_OK)
    {
       rc = E_SUB_ERROR;
       DBG_ERROR("Could not initialize database for cached subscription");
    }   
    if (rc == E_SUB_OK)
    {
       //Update subscription info to the correct uri
       if (!keepuri)
       {
         m_subscriptionInfo.url = m_uri;
       }

       if (m_subscriptionInfo.name == "")
       {
          m_subscriptionInfo.name = m_uri;
       }
       rc = subDb.SetInfoTableData(m_subscriptionInfo);
    }
    if (rc == E_DB_OK)
    {
       rc = subDb.SetTVTableData(m_chanlist);
    }
    
    return rc;
}

wxString CSubscription::GenerateTempFileName()
{
   wxString path = wxFileName(CConfiguration::GetTempDir(), "_dltemp_").GetFullPath();
   DBG_INFO("Temporary file path is %s", (const char*)path.utf8_str());
   return path;
}


void CSubscription::GetCachedData(TSubscriptionList &subList)
{
   wxArrayString cachedFilesArray;
   subList.Clear();
   size_t numCachedFiles = wxDir::GetAllFiles(CConfiguration::GetCacheDir(), &cachedFilesArray, wxEmptyString, wxDIR_FILES);
   for (size_t index = 0; index < numCachedFiles; index++)
   {
      CSubscription subscription("file://" + cachedFilesArray[index]);
      subscription.ReadData();
      subList.Add(subscription);
   }
   subList.Sort(Compare);
}


wxString CSubscription::FindCachedData(wxString url)
{
   wxArrayString cachedFilesArray;
   wxString rcString;
   size_t numCachedFiles = wxDir::GetAllFiles(CConfiguration::GetCacheDir(), &cachedFilesArray, wxEmptyString, wxDIR_FILES);
   for (size_t index = 0; index < numCachedFiles; index++)
   {
      CSubscription subscription("file://" + cachedFilesArray[index]);
      subscription.ReadData();
      if (subscription.GetSubscriptionInfo().url == url)
      {
         rcString = cachedFilesArray[index];
         break;
      }
   }
   return rcString;
}

unsigned int CSubscription::GetNumberOfChannels()
{
   return m_chanlist.GetCount();
}

unsigned int CSubscription::GetNumberOfSources()
{
   unsigned int sources = 0;
   for (size_t i = 0; i < m_chanlist.GetCount(); i++)
   {
      sources += m_chanlist[i].GetStreamURLs().GetCount();
   }
   return sources;   
}

int CSubscription::Compare( CSubscription **item1, CSubscription **item2)
{
  wxString s1 = (*item1)->GetSubscriptionInfo().name;
  wxString s2 = (*item2)->GetSubscriptionInfo().name;

  return strcmp((const char*)s1.utf8_str(), (const char*)s2.utf8_str());
}

