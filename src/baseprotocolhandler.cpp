#include "main.h"
#include "baseprotocolhandler.h"
#include "debug.h"

using namespace tvlite;

CBaseProtocolHandler::CBaseProtocolHandler(wxEvtHandler *parent, wxString url) : 
    m_vlcPlayer(NULL),
    m_pparent(parent),
    m_url(url),
    m_stopped(false)
{
}   

CBaseProtocolHandler::~CBaseProtocolHandler()
{
}

void CBaseProtocolHandler::SetURL(wxString url)
{
   m_url = url;
}

wxString CBaseProtocolHandler::GetURL()
{
   return m_url;
}


void CBaseProtocolHandler::SetVLCPlayer(CVlcPlayer *vlcPlayer)
{
   m_vlcPlayer = vlcPlayer;
}

CVlcPlayer * CBaseProtocolHandler::GetVLCPlayer()
{
   return m_vlcPlayer;
}

void CBaseProtocolHandler::OnStopAsync()
{
       wxCommandEvent exitevent(protEVT_EXIT);
       DBG_INFO("Send exit event");
       wxQueueEvent(m_pparent, exitevent.Clone());
}

bool CBaseProtocolHandler::WasStopped()
{
   return m_stopped;
}
