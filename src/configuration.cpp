#include "configuration.h"
#include <cstdlib> // NULL
#include <wx/string.h> 
#include <wx/stdpaths.h>
#include <wx/filename.h>
#include "debug.h"

#define TEMPDIR_NAME  "temp"
#define CACHEDIR_NAME "cache"
#define CFGFILE_NAME  "config" 
#define LISTDIR_NAME  "lists"

tvlite::CConfiguration* tvlite::CConfiguration::ms_instance = NULL;

tvlite::CConfiguration::CConfiguration()
{
}

tvlite::CConfiguration::~CConfiguration()
{
}

bool tvlite::CConfiguration::Init()
{
   return Init(GetConfigDir()) && Init(GetTempDir()) && Init(GetCacheDir());
}

bool tvlite::CConfiguration::Init(wxString dir)
{  
   DBG_INFO("Checking %s", (const char*)dir.utf8_str());
   wxFileName basedir(dir,"");
   if (!basedir.DirExists())
   {
      if (!wxMkdir(dir))
      {
         DBG_ERROR("Could not make %s directory", (const char*)dir.utf8_str());
         return false;
      }
   }
   
   return true;
}

tvlite::CConfiguration* tvlite::CConfiguration::Instance()
{
   if (ms_instance == NULL) {
      ms_instance = new tvlite::CConfiguration();
   }
   return ms_instance;
}

void tvlite::CConfiguration::Release()
{
   if (ms_instance) {
      delete ms_instance;
   }
   ms_instance = NULL;
}

wxString tvlite::CConfiguration::GetConfigDir()
{
   wxStandardPaths stdPath = wxStandardPaths::Get();
   wxString pathtmp = stdPath.GetUserDataDir(); 
   return pathtmp;
   
}

wxString tvlite::CConfiguration::GetConfigFileName()
{
   wxFileName configFile(GetConfigDir(), "config");
   return configFile.GetFullPath();
}

wxString tvlite::CConfiguration::GetTempDir()
{
   wxFileName tempDir(GetConfigDir(), "");
   tempDir.AppendDir(TEMPDIR_NAME);
   return tempDir.GetFullPath();
   
}

wxString tvlite::CConfiguration::GetCacheDir()
{
   wxFileName cacheDir(GetConfigDir(), "");
   cacheDir.AppendDir(CACHEDIR_NAME);
   return cacheDir.GetFullPath();
}

wxString tvlite::CConfiguration::GetListDir()
{
   wxFileName tempDir(GetConfigDir(), "");
   tempDir.AppendDir(LISTDIR_NAME);
   return tempDir.GetFullPath();
   
}

wxString tvlite::CConfiguration::GetRecordDir()
{
   if (m_recordDir == "")
   {
      wxStandardPaths stdPath = wxStandardPaths::Get();
      m_recordDir = stdPath.GetDocumentsDir();
   }
   return m_recordDir;
}

void tvlite::CConfiguration::SetRecordDir(wxString path)
{
   m_recordDir = path;
}