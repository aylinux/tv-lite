///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "gui.h"

#include "volume.xpm"

///////////////////////////////////////////////////////////////////////////

MainFrameBase::MainFrameBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	m_menuBar = new wxMenuBar( 0 );
	m_menuFile = new wxMenu();
	m_menuManage = new wxMenu();
	wxMenuItem* m_menuManageItem = new wxMenuItem( m_menuFile, wxID_ANY, _("Manage &Lists..."), wxEmptyString, wxITEM_NORMAL, m_menuManage );
	wxMenuItem* m_menuAddLocalList;
	m_menuAddLocalList = new wxMenuItem( m_menuManage, wxID_ANY, wxString( _("Add Local List") ) , wxEmptyString, wxITEM_NORMAL );
	m_menuManage->Append( m_menuAddLocalList );

	m_menuManage->AppendSeparator();

	wxMenuItem* m_menuSaveList;
	m_menuSaveList = new wxMenuItem( m_menuManage, wxID_ANY, wxString( _("Save Whole Channel List As...") ) , wxEmptyString, wxITEM_NORMAL );
	m_menuManage->Append( m_menuSaveList );

	wxMenuItem* m_menuSaveView;
	m_menuSaveView = new wxMenuItem( m_menuManage, wxID_ANY, wxString( _("Save Current View As...") ) , wxEmptyString, wxITEM_NORMAL );
	m_menuManage->Append( m_menuSaveView );

	m_menuManage->AppendSeparator();

	wxMenuItem* m_menuSaveSubscription;
	m_menuSaveSubscription = new wxMenuItem( m_menuManage, wxID_ANY, wxString( _("Save Subscription As Local List") ) , wxEmptyString, wxITEM_NORMAL );
	m_menuManage->Append( m_menuSaveSubscription );

	m_menuManage->AppendSeparator();

	m_menuLocalLists = new wxMenuItem( m_menuManage, wxID_ANY, wxString( _("Manage Local Lists") ) , wxEmptyString, wxITEM_NORMAL );
	m_menuManage->Append( m_menuLocalLists );

	m_menuSubscriptions = new wxMenuItem( m_menuManage, wxID_ANY, wxString( _("Manage Subscriptions") ) , wxEmptyString, wxITEM_NORMAL );
	m_menuManage->Append( m_menuSubscriptions );

	m_menuFile->Append( m_menuManageItem );

	m_menuFile->AppendSeparator();

	wxMenuItem* menuFileExit;
	menuFileExit = new wxMenuItem( m_menuFile, wxID_EXIT, wxString( _("E&xit") ) + wxT('\t') + wxT("Alt+X"), wxEmptyString, wxITEM_NORMAL );
	m_menuFile->Append( menuFileExit );

	m_menuBar->Append( m_menuFile, _("&TVLite") );

	m_menuView = new wxMenu();
	wxMenuItem* m_menuFullScreen;
	m_menuFullScreen = new wxMenuItem( m_menuView, wxID_ANY, wxString( _("Full Screem") ) , wxEmptyString, wxITEM_NORMAL );
	m_menuView->Append( m_menuFullScreen );

	m_menuChannelList = new wxMenuItem( m_menuView, wxID_ANY, wxString( _("Channel List") ) , wxEmptyString, wxITEM_CHECK );
	m_menuView->Append( m_menuChannelList );

	m_menuView->AppendSeparator();

	wxMenu* m_aspectRatio;
	m_aspectRatio = new wxMenu();
	wxMenuItem* m_aspectRatioItem = new wxMenuItem( m_menuView, wxID_ANY, _("Aspect Ratio"), wxEmptyString, wxITEM_NORMAL, m_aspectRatio );
	wxMenuItem* m_aspectRatioDefault;
	m_aspectRatioDefault = new wxMenuItem( m_aspectRatio, wxID_ANY, wxString( _("Default") ) , wxEmptyString, wxITEM_NORMAL );
	m_aspectRatio->Append( m_aspectRatioDefault );

	wxMenuItem* m_aspectRatio169;
	m_aspectRatio169 = new wxMenuItem( m_aspectRatio, wxID_ANY, wxString( _("16:9") ) , wxEmptyString, wxITEM_NORMAL );
	m_aspectRatio->Append( m_aspectRatio169 );

	wxMenuItem* m_aspectRatio43;
	m_aspectRatio43 = new wxMenuItem( m_aspectRatio, wxID_ANY, wxString( _("4:3") ) , wxEmptyString, wxITEM_NORMAL );
	m_aspectRatio->Append( m_aspectRatio43 );

	m_menuView->Append( m_aspectRatioItem );

	m_menuView->AppendSeparator();

	m_videoTracks = new wxMenu();
	wxMenuItem* m_videoTracksItem = new wxMenuItem( m_menuView, wxID_ANY, _("Video Tracks"), wxEmptyString, wxITEM_NORMAL, m_videoTracks );
	m_menuView->Append( m_videoTracksItem );

	m_audioTracks = new wxMenu();
	wxMenuItem* m_audioTracksItem = new wxMenuItem( m_menuView, wxID_ANY, _("Audio Tracks"), wxEmptyString, wxITEM_NORMAL, m_audioTracks );
	m_menuView->Append( m_audioTracksItem );

	m_subtitleTracks = new wxMenu();
	wxMenuItem* m_subtitleTracksItem = new wxMenuItem( m_menuView, wxID_ANY, _("Subtitle Tracks"), wxEmptyString, wxITEM_NORMAL, m_subtitleTracks );
	m_menuView->Append( m_subtitleTracksItem );

	m_menuBar->Append( m_menuView, _("&View") );

	m_helpMenu = new wxMenu();
	wxMenuItem* m_menuAbout;
	m_menuAbout = new wxMenuItem( m_helpMenu, wxID_ANY, wxString( _("About") ) , wxEmptyString, wxITEM_NORMAL );
	m_helpMenu->Append( m_menuAbout );

	m_menuBar->Append( m_helpMenu, _("Help") );

	this->SetMenuBar( m_menuBar );

	wxBoxSizer* mainSizer;
	mainSizer = new wxBoxSizer( wxVERTICAL );

	m_splitter1 = new wxSplitterWindow( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D );
	m_splitter1->Connect( wxEVT_IDLE, wxIdleEventHandler( MainFrameBase::m_splitter1OnIdle ), NULL, this );

	m_panel3 = new wxPanel( m_splitter1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer4;
	bSizer4 = new wxBoxSizer( wxVERTICAL );

	wxString m_listTypeChoices[] = { _("Local Lists"), _("Subscriptions") };
	int m_listTypeNChoices = sizeof( m_listTypeChoices ) / sizeof( wxString );
	m_listType = new wxRadioBox( m_panel3, wxID_ANY, _("List Types"), wxDefaultPosition, wxDefaultSize, m_listTypeNChoices, m_listTypeChoices, 2, wxRA_SPECIFY_COLS );
	m_listType->SetSelection( 0 );
	bSizer4->Add( m_listType, 0, wxALL|wxEXPAND, 5 );

	m_staticText1 = new wxStaticText( m_panel3, wxID_ANY, _("Channel List:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1->Wrap( -1 );
	bSizer4->Add( m_staticText1, 0, wxALL, 5 );

	wxArrayString m_choice1Choices;
	m_choice1 = new wxChoice( m_panel3, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice1Choices, 0 );
	m_choice1->SetSelection( 0 );
	bSizer4->Add( m_choice1, 0, wxALL|wxEXPAND, 5 );

	m_ChannelList = new wxListCtrl( m_panel3, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_NO_HEADER|wxLC_REPORT|wxLC_SINGLE_SEL );
	bSizer4->Add( m_ChannelList, 1, wxALL|wxEXPAND, 5 );

	m_statistics = new wxStaticText( m_panel3, wxID_ANY, _("Total:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_statistics->Wrap( -1 );
	bSizer4->Add( m_statistics, 0, wxALL, 5 );

	wxBoxSizer* bSizer19;
	bSizer19 = new wxBoxSizer( wxHORIZONTAL );

	m_bitmap2 = new wxStaticBitmap( m_panel3, wxID_ANY, wxArtProvider::GetBitmap( wxART_FIND, wxART_OTHER ), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer19->Add( m_bitmap2, 0, wxALIGN_CENTER|wxALL, 5 );

	m_channelSearch = new wxTextCtrl( m_panel3, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	#ifdef __WXGTK__
	if ( !m_channelSearch->HasFlag( wxTE_MULTILINE ) )
	{
	m_channelSearch->SetMaxLength( 64 );
	}
	#else
	m_channelSearch->SetMaxLength( 64 );
	#endif
	bSizer19->Add( m_channelSearch, 1, wxALL, 5 );


	bSizer4->Add( bSizer19, 0, wxEXPAND, 5 );

	wxString m_searchOrderChoices[] = { _("Asc"), _("Desc"), _("None") };
	int m_searchOrderNChoices = sizeof( m_searchOrderChoices ) / sizeof( wxString );
	m_searchOrder = new wxRadioBox( m_panel3, wxID_ANY, _("Order"), wxDefaultPosition, wxDefaultSize, m_searchOrderNChoices, m_searchOrderChoices, 1, wxRA_SPECIFY_ROWS );
	m_searchOrder->SetSelection( 2 );
	bSizer4->Add( m_searchOrder, 0, wxALIGN_CENTER|wxALL|wxEXPAND, 5 );


	m_panel3->SetSizer( bSizer4 );
	m_panel3->Layout();
	bSizer4->Fit( m_panel3 );
	m_panel4 = new wxPanel( m_splitter1, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer3 = new wxBoxSizer( wxVERTICAL );

	m_tvView = new wxPanel( m_panel4, wxID_ANY, wxDefaultPosition, wxSize( 640,-1 ), wxTAB_TRAVERSAL );
	m_tvView->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOW ) );
	m_tvView->SetMinSize( wxSize( 640,360 ) );

	bSizer3->Add( m_tvView, 1, wxEXPAND | wxALL, 0 );

	bSizer41 = new wxBoxSizer( wxHORIZONTAL );

	m_bpButtonPlay = new wxBitmapButton( m_panel4, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|wxBORDER_NONE );

	m_bpButtonPlay->SetBitmap( wxArtProvider::GetBitmap( wxT("gtk-media-play"), wxART_BUTTON ) );
	m_bpButtonPlay->SetBitmapDisabled( wxArtProvider::GetBitmap( wxT("gtk-media-play"), wxART_BUTTON ) );
	m_bpButtonPlay->SetBitmapPressed( wxArtProvider::GetBitmap( wxT("gtk-media-play"), wxART_BUTTON ) );
	m_bpButtonPlay->SetBitmapFocus( wxArtProvider::GetBitmap( wxT("gtk-media-play"), wxART_BUTTON ) );
	m_bpButtonPlay->SetBitmapCurrent( wxArtProvider::GetBitmap( wxT("gtk-media-play"), wxART_BUTTON ) );
	bSizer41->Add( m_bpButtonPlay, 0, wxALIGN_CENTER|wxALL, 5 );

	m_bpButtonStop = new wxBitmapButton( m_panel4, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|wxBORDER_NONE );

	m_bpButtonStop->SetBitmap( wxArtProvider::GetBitmap( wxT("gtk-media-stop"), wxART_BUTTON ) );
	m_bpButtonStop->SetBitmapDisabled( wxArtProvider::GetBitmap( wxT("gtk-media-stop"), wxART_BUTTON ) );
	m_bpButtonStop->SetBitmapPressed( wxArtProvider::GetBitmap( wxT("gtk-media-stop"), wxART_BUTTON ) );
	bSizer41->Add( m_bpButtonStop, 0, wxALIGN_CENTER|wxALL, 5 );

	m_bpButtonPause = new wxBitmapButton( m_panel4, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|wxBORDER_NONE );

	m_bpButtonPause->SetBitmap( wxArtProvider::GetBitmap( wxT("gtk-media-pause"), wxART_BUTTON ) );
	m_bpButtonPause->SetBitmapDisabled( wxArtProvider::GetBitmap( wxT("gtk-media-pause"), wxART_BUTTON ) );
	m_bpButtonPause->SetBitmapPressed( wxArtProvider::GetBitmap( wxT("gtk-media-pause"), wxART_BUTTON ) );
	m_bpButtonPause->SetBitmapFocus( wxArtProvider::GetBitmap( wxT("gtk-media-pause"), wxART_BUTTON ) );
	m_bpButtonPause->SetBitmapCurrent( wxArtProvider::GetBitmap( wxT("gtk-media-pause"), wxART_BUTTON ) );
	bSizer41->Add( m_bpButtonPause, 0, wxALIGN_CENTER|wxALL, 5 );

	m_bpButtonRecord = new wxBitmapButton( m_panel4, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|wxBORDER_NONE );

	m_bpButtonRecord->SetBitmap( wxArtProvider::GetBitmap( wxT("gtk-media-record"), wxART_BUTTON ) );
	m_bpButtonRecord->SetBitmapDisabled( wxArtProvider::GetBitmap( wxT("gtk-media-record"), wxART_BUTTON ) );
	m_bpButtonRecord->SetBitmapPressed( wxArtProvider::GetBitmap( wxT("gtk-media-record"), wxART_BUTTON ) );
	m_bpButtonRecord->SetBitmapFocus( wxArtProvider::GetBitmap( wxT("gtk-media-record"), wxART_BUTTON ) );
	m_bpButtonRecord->SetBitmapCurrent( wxArtProvider::GetBitmap( wxT("gtk-media-record"), wxART_BUTTON ) );
	bSizer41->Add( m_bpButtonRecord, 0, wxALIGN_CENTER|wxALL, 5 );

	m_movieslider = new wxSlider( m_panel4, wxID_ANY, 0, 0, 10000, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL|wxBORDER_NONE );
	bSizer41->Add( m_movieslider, 1, wxALIGN_CENTER|wxALL, 0 );

	m_bitmap1 = new wxStaticBitmap( m_panel4, wxID_ANY, wxBitmap( volume_xpm ), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	bSizer41->Add( m_bitmap1, 0, wxALIGN_CENTER|wxALL, 5 );

	m_slider1 = new wxSlider( m_panel4, wxID_ANY, 80, 0, 100, wxDefaultPosition, wxSize( 100,-1 ), wxSL_HORIZONTAL );
	bSizer41->Add( m_slider1, 0, wxALIGN_CENTER|wxALL, 5 );

	m_bpButtonFullScreen = new wxBitmapButton( m_panel4, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|wxBORDER_NONE );

	m_bpButtonFullScreen->SetBitmap( wxArtProvider::GetBitmap( wxT("gtk-fullscreen"), wxART_BUTTON ) );
	bSizer41->Add( m_bpButtonFullScreen, 0, wxALIGN_CENTER|wxALIGN_RIGHT|wxALL, 5 );


	bSizer3->Add( bSizer41, 0, wxEXPAND, 5 );


	m_panel4->SetSizer( bSizer3 );
	m_panel4->Layout();
	bSizer3->Fit( m_panel4 );
	m_splitter1->SplitVertically( m_panel3, m_panel4, 300 );
	mainSizer->Add( m_splitter1, 1, wxEXPAND, 5 );


	this->SetSizer( mainSizer );
	this->Layout();
	m_statusBar = this->CreateStatusBar( 3, wxSTB_SIZEGRIP|wxBORDER_SUNKEN, wxID_ANY );
	m_trtimer.SetOwner( this, wxID_ANY );

	this->Centre( wxBOTH );

	// Connect Events
	this->Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( MainFrameBase::OnCloseFrame ) );
	this->Connect( wxEVT_SHOW, wxShowEventHandler( MainFrameBase::OnShowFrame ) );
	m_menuManage->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnLocalListAdd ), this, m_menuAddLocalList->GetId());
	m_menuManage->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnSaveList ), this, m_menuSaveList->GetId());
	m_menuManage->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnSaveView ), this, m_menuSaveView->GetId());
	m_menuManage->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnSaveSubscriptionAsLocalList ), this, m_menuSaveSubscription->GetId());
	m_menuManage->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnLocalListClick ), this, m_menuLocalLists->GetId());
	m_menuManage->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnSubscriptionClick ), this, m_menuSubscriptions->GetId());
	m_menuFile->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnExitClick ), this, menuFileExit->GetId());
	m_menuView->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnFullScreen ), this, m_menuFullScreen->GetId());
	m_menuView->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::ToggleChannelList ), this, m_menuChannelList->GetId());
	m_aspectRatio->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnARDefault ), this, m_aspectRatioDefault->GetId());
	m_aspectRatio->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnAR169 ), this, m_aspectRatio169->GetId());
	m_aspectRatio->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnAR43 ), this, m_aspectRatio43->GetId());
	m_helpMenu->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnAbout ), this, m_menuAbout->GetId());
	m_listType->Connect( wxEVT_COMMAND_RADIOBOX_SELECTED, wxCommandEventHandler( MainFrameBase::OnListTypeClick ), NULL, this );
	m_choice1->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( MainFrameBase::OnSubscriptionSelected ), NULL, this );
	m_ChannelList->Connect( wxEVT_COMMAND_LIST_ITEM_ACTIVATED, wxListEventHandler( MainFrameBase::OnListPlay ), NULL, this );
	m_ChannelList->Connect( wxEVT_COMMAND_LIST_ITEM_RIGHT_CLICK, wxListEventHandler( MainFrameBase::OnListContextMenu ), NULL, this );
	m_channelSearch->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainFrameBase::OnPopulateChannel ), NULL, this );
	m_channelSearch->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( MainFrameBase::OnPopulateChannel ), NULL, this );
	m_searchOrder->Connect( wxEVT_COMMAND_RADIOBOX_SELECTED, wxCommandEventHandler( MainFrameBase::OnPopulateChannel ), NULL, this );
	m_tvView->Connect( wxEVT_CHAR, wxKeyEventHandler( MainFrameBase::KeyPressed ), NULL, this );
	m_tvView->Connect( wxEVT_LEFT_DCLICK, wxMouseEventHandler( MainFrameBase::ToggleFullScreen ), NULL, this );
	m_bpButtonPlay->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnPlay ), NULL, this );
	m_bpButtonStop->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnStop ), NULL, this );
	m_bpButtonPause->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnPause ), NULL, this );
	m_bpButtonRecord->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnRecord ), NULL, this );
	m_movieslider->Connect( wxEVT_LEFT_DOWN, wxMouseEventHandler( MainFrameBase::OnSeekMouseChanged ), NULL, this );
	m_movieslider->Connect( wxEVT_SLIDER, wxCommandEventHandler( MainFrameBase::OnSeekUsrChanged ), NULL, this );
	m_slider1->Connect( wxEVT_SLIDER, wxCommandEventHandler( MainFrameBase::OnVolumeChanged ), NULL, this );
	m_bpButtonFullScreen->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnFullScreen ), NULL, this );
	this->Connect( wxID_ANY, wxEVT_TIMER, wxTimerEventHandler( MainFrameBase::HideBar ) );
}

MainFrameBase::~MainFrameBase()
{
	// Disconnect Events
	this->Disconnect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( MainFrameBase::OnCloseFrame ) );
	this->Disconnect( wxEVT_SHOW, wxShowEventHandler( MainFrameBase::OnShowFrame ) );
	m_listType->Disconnect( wxEVT_COMMAND_RADIOBOX_SELECTED, wxCommandEventHandler( MainFrameBase::OnListTypeClick ), NULL, this );
	m_choice1->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( MainFrameBase::OnSubscriptionSelected ), NULL, this );
	m_ChannelList->Disconnect( wxEVT_COMMAND_LIST_ITEM_ACTIVATED, wxListEventHandler( MainFrameBase::OnListPlay ), NULL, this );
	m_ChannelList->Disconnect( wxEVT_COMMAND_LIST_ITEM_RIGHT_CLICK, wxListEventHandler( MainFrameBase::OnListContextMenu ), NULL, this );
	m_channelSearch->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainFrameBase::OnPopulateChannel ), NULL, this );
	m_channelSearch->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( MainFrameBase::OnPopulateChannel ), NULL, this );
	m_searchOrder->Disconnect( wxEVT_COMMAND_RADIOBOX_SELECTED, wxCommandEventHandler( MainFrameBase::OnPopulateChannel ), NULL, this );
	m_tvView->Disconnect( wxEVT_CHAR, wxKeyEventHandler( MainFrameBase::KeyPressed ), NULL, this );
	m_tvView->Disconnect( wxEVT_LEFT_DCLICK, wxMouseEventHandler( MainFrameBase::ToggleFullScreen ), NULL, this );
	m_bpButtonPlay->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnPlay ), NULL, this );
	m_bpButtonStop->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnStop ), NULL, this );
	m_bpButtonPause->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnPause ), NULL, this );
	m_bpButtonRecord->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnRecord ), NULL, this );
	m_movieslider->Disconnect( wxEVT_LEFT_DOWN, wxMouseEventHandler( MainFrameBase::OnSeekMouseChanged ), NULL, this );
	m_movieslider->Disconnect( wxEVT_SLIDER, wxCommandEventHandler( MainFrameBase::OnSeekUsrChanged ), NULL, this );
	m_slider1->Disconnect( wxEVT_SLIDER, wxCommandEventHandler( MainFrameBase::OnVolumeChanged ), NULL, this );
	m_bpButtonFullScreen->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnFullScreen ), NULL, this );
	this->Disconnect( wxID_ANY, wxEVT_TIMER, wxTimerEventHandler( MainFrameBase::HideBar ) );

}

CRecordChoiceBase::CRecordChoiceBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer18;
	bSizer18 = new wxBoxSizer( wxVERTICAL );

	wxString m_recordChoiceBoxChoices[] = { _("None"), _("Record"), _("Stream") };
	int m_recordChoiceBoxNChoices = sizeof( m_recordChoiceBoxChoices ) / sizeof( wxString );
	m_recordChoiceBox = new wxRadioBox( this, wxID_ANY, _("Recording Options"), wxDefaultPosition, wxDefaultSize, m_recordChoiceBoxNChoices, m_recordChoiceBoxChoices, 1, wxRA_SPECIFY_COLS|wxBORDER_NONE );
	m_recordChoiceBox->SetSelection( 1 );
	bSizer18->Add( m_recordChoiceBox, 1, wxALIGN_CENTER|wxALL|wxEXPAND, 5 );


	this->SetSizer( bSizer18 );
	this->Layout();
	bSizer18->Fit( this );

	this->Centre( wxBOTH );

	// Connect Events
	this->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CRecordChoiceBase::OnLeave ) );
	m_recordChoiceBox->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CRecordChoiceBase::OnLeaveRadioBox ), NULL, this );
}

CRecordChoiceBase::~CRecordChoiceBase()
{
	// Disconnect Events
	this->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CRecordChoiceBase::OnLeave ) );
	m_recordChoiceBox->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CRecordChoiceBase::OnLeaveRadioBox ), NULL, this );

}

CLocalListDialogBase::CLocalListDialogBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer10;
	bSizer10 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer11;
	bSizer11 = new wxBoxSizer( wxHORIZONTAL );

	m_LocalDataList = new wxListCtrl( this, wxID_ANY, wxDefaultPosition, wxSize( 300,400 ), wxLC_NO_HEADER|wxLC_REPORT|wxLC_SINGLE_SEL );
	bSizer11->Add( m_LocalDataList, 0, wxALL, 5 );

	wxBoxSizer* bSizer12;
	bSizer12 = new wxBoxSizer( wxVERTICAL );

	m_newButton = new wxButton( this, wxID_ANY, _("New"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer12->Add( m_newButton, 0, wxALL, 5 );

	m_editButton = new wxButton( this, wxID_ANY, _("Edit"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer12->Add( m_editButton, 0, wxALL, 5 );

	m_deleteButton = new wxButton( this, wxID_ANY, _("Delete"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer12->Add( m_deleteButton, 0, wxALL, 5 );


	bSizer11->Add( bSizer12, 1, wxEXPAND, 5 );


	bSizer10->Add( bSizer11, 1, wxEXPAND, 5 );

	m_closeButton = new wxButton( this, wxID_OK, _("Close"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer10->Add( m_closeButton, 0, wxALIGN_RIGHT|wxALL, 5 );


	this->SetSizer( bSizer10 );
	this->Layout();
	bSizer10->Fit( this );

	this->Centre( wxBOTH );

	// Connect Events
	m_newButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CLocalListDialogBase::OnNewClick ), NULL, this );
	m_editButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CLocalListDialogBase::OnEditClick ), NULL, this );
	m_deleteButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CLocalListDialogBase::OnDeleteClick ), NULL, this );
}

CLocalListDialogBase::~CLocalListDialogBase()
{
	// Disconnect Events
	m_newButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CLocalListDialogBase::OnNewClick ), NULL, this );
	m_editButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CLocalListDialogBase::OnEditClick ), NULL, this );
	m_deleteButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CLocalListDialogBase::OnDeleteClick ), NULL, this );

}

CSubscriptionListDialogBase::CSubscriptionListDialogBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer5;
	bSizer5 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer6;
	bSizer6 = new wxBoxSizer( wxHORIZONTAL );

	m_SubscriptionDataList = new wxDataViewListCtrl( this, wxID_ANY, wxDefaultPosition, wxSize( 600,400 ), 0 );
	bSizer6->Add( m_SubscriptionDataList, 1, wxALL|wxEXPAND, 5 );

	wxBoxSizer* bSizer7;
	bSizer7 = new wxBoxSizer( wxVERTICAL );

	m_newButton = new wxButton( this, wxID_ANY, _("New"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer7->Add( m_newButton, 0, wxALL, 5 );


	bSizer7->Add( 0, 20, 0, wxALL, 0 );

	m_updateButton = new wxButton( this, wxID_ANY, _("Update"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer7->Add( m_updateButton, 0, wxALL, 5 );

	m_editButton = new wxButton( this, wxID_ANY, _("Edit"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer7->Add( m_editButton, 0, wxALL, 5 );

	m_deleteButton = new wxButton( this, wxID_ANY, _("Delete"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer7->Add( m_deleteButton, 0, wxALL, 5 );


	bSizer6->Add( bSizer7, 0, wxEXPAND, 5 );


	bSizer5->Add( bSizer6, 1, wxEXPAND, 5 );

	m_close = new wxButton( this, wxID_OK, _("Close"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer5->Add( m_close, 0, wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxALL, 5 );


	this->SetSizer( bSizer5 );
	this->Layout();
	bSizer5->Fit( this );

	this->Centre( wxBOTH );

	// Connect Events
	m_newButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionListDialogBase::OnNewClicked ), NULL, this );
	m_updateButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionListDialogBase::OnUpdateButtonClick ), NULL, this );
	m_editButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionListDialogBase::OnEditClicked ), NULL, this );
	m_deleteButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionListDialogBase::OnDeleteClicked ), NULL, this );
}

CSubscriptionListDialogBase::~CSubscriptionListDialogBase()
{
	// Disconnect Events
	m_newButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionListDialogBase::OnNewClicked ), NULL, this );
	m_updateButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionListDialogBase::OnUpdateButtonClick ), NULL, this );
	m_editButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionListDialogBase::OnEditClicked ), NULL, this );
	m_deleteButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionListDialogBase::OnDeleteClicked ), NULL, this );

}

CSubscriptionDialogBase::CSubscriptionDialogBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxSize( -1,-1 ), wxSize( -1,-1 ) );

	wxBoxSizer* bSizer8;
	bSizer8 = new wxBoxSizer( wxVERTICAL );

	m_NameLabel = new wxStaticText( this, wxID_ANY, _("Name"), wxDefaultPosition, wxDefaultSize, 0 );
	m_NameLabel->Wrap( -1 );
	bSizer8->Add( m_NameLabel, 0, wxALL, 5 );

	m_Name = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer8->Add( m_Name, 0, wxALL|wxEXPAND, 5 );

	m_staticText2 = new wxStaticText( this, wxID_ANY, _("URL"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2->Wrap( -1 );
	bSizer8->Add( m_staticText2, 0, wxALL, 5 );

	wxBoxSizer* bSizer9;
	bSizer9 = new wxBoxSizer( wxHORIZONTAL );

	m_URLText = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer9->Add( m_URLText, 1, wxALL, 5 );

	m_PathButton = new wxButton( this, wxID_ANY, _(" ... "), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer9->Add( m_PathButton, 0, wxALL, 5 );


	bSizer8->Add( bSizer9, 1, wxALL|wxEXPAND, 5 );

	m_sdbSizer2 = new wxStdDialogButtonSizer();
	m_sdbSizer2OK = new wxButton( this, wxID_OK );
	m_sdbSizer2->AddButton( m_sdbSizer2OK );
	m_sdbSizer2Cancel = new wxButton( this, wxID_CANCEL );
	m_sdbSizer2->AddButton( m_sdbSizer2Cancel );
	m_sdbSizer2->Realize();

	bSizer8->Add( m_sdbSizer2, 1, wxALL|wxEXPAND, 5 );


	this->SetSizer( bSizer8 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_PathButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionDialogBase::OnBrowseButtonClick ), NULL, this );
	m_sdbSizer2Cancel->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionDialogBase::OnCancelButtonClicked ), NULL, this );
	m_sdbSizer2OK->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionDialogBase::OnOkButtonClicked ), NULL, this );
}

CSubscriptionDialogBase::~CSubscriptionDialogBase()
{
	// Disconnect Events
	m_PathButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionDialogBase::OnBrowseButtonClick ), NULL, this );
	m_sdbSizer2Cancel->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionDialogBase::OnCancelButtonClicked ), NULL, this );
	m_sdbSizer2OK->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionDialogBase::OnOkButtonClicked ), NULL, this );

}

CLocalDialogBase::CLocalDialogBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer13;
	bSizer13 = new wxBoxSizer( wxVERTICAL );

	m_NameLabel = new wxStaticText( this, wxID_ANY, _("Name"), wxDefaultPosition, wxDefaultSize, 0 );
	m_NameLabel->Wrap( -1 );
	bSizer13->Add( m_NameLabel, 0, wxALL, 5 );

	m_Name = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 500,-1 ), 0 );
	bSizer13->Add( m_Name, 0, wxALL|wxEXPAND, 5 );

	m_AuthorLabel = new wxStaticText( this, wxID_ANY, _("Author"), wxDefaultPosition, wxDefaultSize, 0 );
	m_AuthorLabel->Wrap( -1 );
	bSizer13->Add( m_AuthorLabel, 0, wxALL, 5 );

	m_Author = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 500,-1 ), 0 );
	bSizer13->Add( m_Author, 0, wxALL|wxEXPAND, 5 );

	m_URLLabel = new wxStaticText( this, wxID_ANY, _("URL"), wxDefaultPosition, wxDefaultSize, 0 );
	m_URLLabel->Wrap( -1 );
	bSizer13->Add( m_URLLabel, 0, wxALL, 5 );

	m_urlText = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 500,-1 ), 0 );
	bSizer13->Add( m_urlText, 0, wxALL|wxEXPAND, 5 );

	m_VersionLabel = new wxStaticText( this, wxID_ANY, _("Version"), wxDefaultPosition, wxDefaultSize, 0 );
	m_VersionLabel->Wrap( -1 );
	bSizer13->Add( m_VersionLabel, 0, wxALL, 5 );

	m_versionText = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 500,-1 ), 0 );
	bSizer13->Add( m_versionText, 0, wxALL|wxEXPAND, 5 );

	m_sdbSizer2 = new wxStdDialogButtonSizer();
	m_sdbSizer2OK = new wxButton( this, wxID_OK );
	m_sdbSizer2->AddButton( m_sdbSizer2OK );
	m_sdbSizer2Cancel = new wxButton( this, wxID_CANCEL );
	m_sdbSizer2->AddButton( m_sdbSizer2Cancel );
	m_sdbSizer2->Realize();

	bSizer13->Add( m_sdbSizer2, 1, wxALL|wxEXPAND, 5 );


	this->SetSizer( bSizer13 );
	this->Layout();
	bSizer13->Fit( this );

	this->Centre( wxBOTH );

	// Connect Events
	m_sdbSizer2OK->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CLocalDialogBase::OnOkClicked ), NULL, this );
}

CLocalDialogBase::~CLocalDialogBase()
{
	// Disconnect Events
	m_sdbSizer2OK->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CLocalDialogBase::OnOkClicked ), NULL, this );

}

CChannelEditBase::CChannelEditBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer14;
	bSizer14 = new wxBoxSizer( wxVERTICAL );

	m_staticText8 = new wxStaticText( this, wxID_ANY, _("Channel Name"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText8->Wrap( -1 );
	bSizer14->Add( m_staticText8, 0, wxALL, 5 );

	m_channelName = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 600,-1 ), 0 );
	bSizer14->Add( m_channelName, 0, wxALL|wxEXPAND, 5 );

	m_staticText9 = new wxStaticText( this, wxID_ANY, _("Channel Addresses"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText9->Wrap( -1 );
	bSizer14->Add( m_staticText9, 0, wxALL, 5 );

	wxBoxSizer* bSizer15;
	bSizer15 = new wxBoxSizer( wxHORIZONTAL );

	m_addressList = new wxDataViewListCtrl( this, wxID_ANY, wxDefaultPosition, wxSize( 600,200 ), 0 );
	bSizer15->Add( m_addressList, 1, wxALL|wxEXPAND, 5 );

	wxBoxSizer* bSizer16;
	bSizer16 = new wxBoxSizer( wxVERTICAL );

	m_addButton = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	m_addButton->SetBitmap( wxArtProvider::GetBitmap( wxT("gtk-add"), wxART_BUTTON ) );
	m_addButton->SetBitmapDisabled( wxArtProvider::GetBitmap( wxT("gtk-add"), wxART_BUTTON ) );
	m_addButton->SetBitmapPressed( wxArtProvider::GetBitmap( wxT("gtk-add"), wxART_BUTTON ) );
	m_addButton->SetBitmapFocus( wxArtProvider::GetBitmap( wxT("gtk-add"), wxART_BUTTON ) );
	m_addButton->SetBitmapCurrent( wxArtProvider::GetBitmap( wxT("gtk-add"), wxART_BUTTON ) );
	bSizer16->Add( m_addButton, 0, wxALL, 5 );

	m_deleteButton = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	m_deleteButton->SetBitmap( wxArtProvider::GetBitmap( wxT("gtk-remove"), wxART_BUTTON ) );
	m_deleteButton->SetBitmapDisabled( wxArtProvider::GetBitmap( wxT("gtk-remove"), wxART_BUTTON ) );
	m_deleteButton->SetBitmapPressed( wxArtProvider::GetBitmap( wxT("gtk-remove"), wxART_BUTTON ) );
	m_deleteButton->SetBitmapFocus( wxArtProvider::GetBitmap( wxT("gtk-remove"), wxART_BUTTON ) );
	m_deleteButton->SetBitmapCurrent( wxArtProvider::GetBitmap( wxT("gtk-remove"), wxART_BUTTON ) );
	bSizer16->Add( m_deleteButton, 0, wxALL, 5 );

	m_upButton = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	m_upButton->SetBitmap( wxArtProvider::GetBitmap( wxART_GO_UP, wxART_BUTTON ) );
	m_upButton->SetBitmapDisabled( wxArtProvider::GetBitmap( wxART_GO_UP, wxART_BUTTON ) );
	m_upButton->SetBitmapPressed( wxArtProvider::GetBitmap( wxART_GO_UP, wxART_BUTTON ) );
	m_upButton->SetBitmapFocus( wxArtProvider::GetBitmap( wxART_GO_UP, wxART_BUTTON ) );
	m_upButton->SetBitmapCurrent( wxArtProvider::GetBitmap( wxART_GO_UP, wxART_BUTTON ) );
	bSizer16->Add( m_upButton, 0, wxALL, 5 );

	m_downButton = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	m_downButton->SetBitmap( wxArtProvider::GetBitmap( wxART_GO_DOWN, wxART_BUTTON ) );
	m_downButton->SetBitmapDisabled( wxArtProvider::GetBitmap( wxART_GO_DOWN, wxART_BUTTON ) );
	m_downButton->SetBitmapPressed( wxArtProvider::GetBitmap( wxART_GO_DOWN, wxART_BUTTON ) );
	m_downButton->SetBitmapFocus( wxArtProvider::GetBitmap( wxART_GO_DOWN, wxART_BUTTON ) );
	m_downButton->SetBitmapCurrent( wxArtProvider::GetBitmap( wxART_GO_DOWN, wxART_BUTTON ) );
	bSizer16->Add( m_downButton, 0, wxALL, 5 );


	bSizer15->Add( bSizer16, 0, wxEXPAND, 5 );


	bSizer14->Add( bSizer15, 1, wxEXPAND, 5 );

	m_sdbSizer3 = new wxStdDialogButtonSizer();
	m_sdbSizer3OK = new wxButton( this, wxID_OK );
	m_sdbSizer3->AddButton( m_sdbSizer3OK );
	m_sdbSizer3Cancel = new wxButton( this, wxID_CANCEL );
	m_sdbSizer3->AddButton( m_sdbSizer3Cancel );
	m_sdbSizer3->Realize();

	bSizer14->Add( m_sdbSizer3, 0, wxALL|wxEXPAND, 5 );


	this->SetSizer( bSizer14 );
	this->Layout();
	bSizer14->Fit( this );

	this->Centre( wxBOTH );

	// Connect Events
	m_addButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CChannelEditBase::OnAddAddress ), NULL, this );
	m_deleteButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CChannelEditBase::OnDeleteAddress ), NULL, this );
	m_upButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CChannelEditBase::OnUpClicked ), NULL, this );
	m_downButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CChannelEditBase::OnDownClicked ), NULL, this );
	m_sdbSizer3OK->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CChannelEditBase::OnOkClicked ), NULL, this );
}

CChannelEditBase::~CChannelEditBase()
{
	// Disconnect Events
	m_addButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CChannelEditBase::OnAddAddress ), NULL, this );
	m_deleteButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CChannelEditBase::OnDeleteAddress ), NULL, this );
	m_upButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CChannelEditBase::OnUpClicked ), NULL, this );
	m_downButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CChannelEditBase::OnDownClicked ), NULL, this );
	m_sdbSizer3OK->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CChannelEditBase::OnOkClicked ), NULL, this );

}

CDlProgressBase::CDlProgressBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer17;
	bSizer17 = new wxBoxSizer( wxVERTICAL );

	m_urlText = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_urlText->Wrap( -1 );
	bSizer17->Add( m_urlText, 0, wxALL|wxBOTTOM, 5 );

	m_progressGauge = new wxGauge( this, wxID_ANY, 100, wxDefaultPosition, wxSize( 380,-1 ), wxGA_HORIZONTAL|wxGA_SMOOTH );
	m_progressGauge->SetValue( 0 );
	bSizer17->Add( m_progressGauge, 1, wxALL|wxEXPAND, 5 );

	m_sdbSizer4 = new wxStdDialogButtonSizer();
	m_sdbSizer4Cancel = new wxButton( this, wxID_CANCEL );
	m_sdbSizer4->AddButton( m_sdbSizer4Cancel );
	m_sdbSizer4->Realize();

	bSizer17->Add( m_sdbSizer4, 1, wxEXPAND, 5 );


	this->SetSizer( bSizer17 );
	this->Layout();
	bSizer17->Fit( this );

	this->Centre( wxBOTH );

	// Connect Events
	this->Connect( wxEVT_INIT_DIALOG, wxInitDialogEventHandler( CDlProgressBase::Init ) );
	m_sdbSizer4Cancel->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CDlProgressBase::OnCancelClicked ), NULL, this );
}

CDlProgressBase::~CDlProgressBase()
{
	// Disconnect Events
	this->Disconnect( wxEVT_INIT_DIALOG, wxInitDialogEventHandler( CDlProgressBase::Init ) );
	m_sdbSizer4Cancel->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CDlProgressBase::OnCancelClicked ), NULL, this );

}
