#include "acestreamenginehandler.h"
#include "debug.h"

#define E_ERROR -1
#define E_OK 0


using namespace tvlite;


CAcestreamEngineHandler::CAcestreamEngineHandler(wxEvtHandler *parent, wxString url) : 
      CBaseProtocolHandler(parent, url), m_IsInstalled(false)
      
{

    Connect(wxID_ANY, wxEVT_END_PROCESS, wxProcessEventHandler(CAcestreamEngineHandler::OnExitAceProcess));
}

CAcestreamEngineHandler::~CAcestreamEngineHandler()
{
   DBG_INFO("ACESTREAM Protocol Handler -> delete")
   if (m_process)
   {
      delete (m_process);
      m_process = NULL;
   }
}


void CAcestreamEngineHandler::Start()
{
   int result = E_OK;
   m_process = new CAceProtocol(this);
   if (!m_process)
   {
      DBG_ERROR("Could not create acestream process")
      result = E_ERROR;
   }
   if (result == E_OK)
   {
      result = m_process->StartProtocol(m_protopid);
      m_IsInstalled = m_process->IsInstalled();
   }
   else
   {
      DBG_ERROR("Could not start acestream engine");
   }

}
   
void CAcestreamEngineHandler::OnExitAceProcess(wxProcessEvent &event)
{
   
   if ((long)event.GetPid() != m_protopid)
   {
      event.Skip();
      return;
   }
   
   DBG_ERROR("Acestream process has exit.");
   delete m_process;
   m_process = NULL;
   OnStopAsync();
}   



void CAcestreamEngineHandler::Stop()
{
   if (m_process)
   {
      DBG_INFO ("m_process not null");
      m_process->StopProtocol();
   }
   else
   {
      OnStopAsync();
   }   
}


