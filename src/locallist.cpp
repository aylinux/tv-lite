#include <wx/wx.h> 
#include <wx/filename.h>
#include <wx/dir.h>
#include "locallist.h"
#include "configuration.h"

using namespace tvlite;

CLocalList::CLocalList()
{

}

CLocalList::CLocalList(wxString uri):CSubscription(uri)
{

}

CLocalList::~CLocalList()
{

}

int CLocalList::ReadData()
{
   m_chanlist.Clear();
   return ParseDbData(m_uri);
}


void CLocalList::GetStoredData(TLocalLists &locLists)
{
   wxArrayString cachedFilesArray;
   locLists.Clear();
   size_t numCachedFiles = wxDir::GetAllFiles(CConfiguration::GetListDir(), &cachedFilesArray, wxEmptyString, wxDIR_FILES);
   for (size_t index = 0; index < numCachedFiles; index++)
   {
      CLocalList locList(cachedFilesArray[index]);
      locList.ReadData();
      locLists.Add(locList);
   }
   locLists.Sort(Compare);

}

int CLocalList::UpdateChannel(CChannel channel)
{
   CDataBase db(m_uri);
   int rc = db.Init();
   if (rc == E_DB_OK)
   {
      rc = db.UpdateChannel(channel);
   }
   return rc; 
}


int CLocalList::AddChannel(CChannel channel)
{
   CDataBase db(m_uri);
   int rc = db.Init();
   if (rc == E_DB_OK)
   {
      rc = db.AddChannel(channel);
   }
   return rc; 
}


int CLocalList::DeleteChannel(CChannel channel)
{
   CDataBase db(m_uri);
   int rc = db.Init();
   if (rc == E_DB_OK)
   {
      rc = db.DeleteChannel(channel);
   }
   return rc; 
}

int CLocalList::UpdateInfo(CSubscriptionInfo &info)
{
   CDataBase db(m_uri);
   int rc = db.Init();
   if (rc == E_DB_OK)
   {
      rc = db.UpdateInfoTableData(info);
   }
   return rc; 
}

int CLocalList::Compare( CLocalList **item1, CLocalList **item2)
{
  //magarie, dar merge
   return CSubscription::Compare((CSubscription**)item1, (CSubscription**)item2);
}