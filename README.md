# TV-Lite
This is work in progress. About 70-80% functional

To build

Install deppendencies first

The following command will do this for you, on Ubuntu:

`sudo apt install build-essential cmake libsqlite3-0 libsqlite3-dev rapidjson-dev libcurl4 libcurl4-openssl-dev libwxgtk3.0-gtk3-0v5 libwxgtk3.0-gtk3-dev libgtk-3-0 libgtk-3-dev libvlc5 libvlc-dev vlc libuuid1 uuid-dev`

Unpack the archive in a folder of your choice. Once unpacked, execute the following:

`cd src`

`cmake .`

`make`

`make install`

